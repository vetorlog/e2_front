import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PopulateDataCollectUtil } from 'src/app/shared/utils/populate-data-collect';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { DialogDataCollectComponent } from 'src/app/shared/dialog-modal/dialog-data-collect/dialog-data-collect.component';
import { DialogDataCollectModel } from 'src/app/shared/model/dialog-data-collect';

@Component({
  selector: 'emeter2-data-collect',
  templateUrl: './data-collect.component.html',
  styleUrls: ['./data-collect.component.scss']
})
export class DataCollectComponent implements OnInit {

  constructor(private populateDC: PopulateDataCollectUtil, private toastr: ToastrService, public dialog: MatDialog) { }

  dateInitial: Date;
  dateFinally: Date;
  formCollect: FormGroup;
  objHeader = [];
  objValue = [];
  showResum: boolean = false;
  showFullDataTable: boolean = false;
  showTable: boolean = false;
  objMain: any = [];

  ngOnInit(): void {
  }

  openModal() {
    var teste: DialogDataCollectModel = new DialogDataCollectModel;
    teste.title = "Energia";
    teste.details.push({ title: 'Consumo Ponta', value: '0,000 kWh' })
    teste.details.push({ title: 'Consumo Fora', value: '12,471 kWh' })
    teste.details.push({ title: 'Geração Ponta', value: '47,120 MWh' })
    teste.details.push({ title: 'Geração Fora', value: '718,629 MWh' });

    this.dialog.open(DialogDataCollectComponent, { data: teste })
  }

  getResumData(data) {
    if (data != null) {
      this.showResum = true;
      if (data.typeMeter == "energy") {
        this.objMain.objResumOne = this.populateDC.populateResume("Energia", data);
        this.objMain.objModalOne = this.populateDC.populateModalResume("Energia", data);

        this.objMain.objResumSecond = this.populateDC.populateResume("Reativo", data);
        this.objMain.objModalSecond = this.populateDC.populateModalResume("Reativo", data);

        this.objMain.objResumThird = this.populateDC.populateResume("Demanda", data);
        this.objMain.objModalThird = this.populateDC.populateModalResume("Demanda", data);

        [this.objMain.guarantee, this.objMain.averageGenerated] = this.populateDC.populateGuarantee(data);
      } else if (data.typeMeter == "scde") {
        this.objMain.objResumOne = this.populateDC.populateResume("Energia SCDE", data);
        this.objMain.objModalOne = this.populateDC.populateModalResume("Energia SCDE", data);

        this.objMain.objResumSecond = this.populateDC.populateResume("Reativo SCDE", data);
        this.objMain.objModalSecond = this.populateDC.populateModalResume("Reativo", data);

        this.objMain.objResumThird = this.populateDC.populateResume("Demanda SCDE", data);
        this.objMain.objModalThird = this.populateDC.populateModalResume("Demanda", data);

        [this.objMain.guarantee, this.objMain.averageGenerated] = this.populateDC.populateGuarantee(data);
      }
      else {
        this.objMain.objResumOne = this.populateDC.populateResume("Chuva", data);
        this.objMain.objModalOne = this.populateDC.populateModalResume("Chuva", data);

        this.objMain.objResumSecond = this.populateDC.populateResume("Nivel", data);
        this.objMain.objModalSecond = this.populateDC.populateModalResume("Nivel", data);

        this.objMain.objResumThird = this.populateDC.populateResume("Vazao", data);
        this.objMain.objModalThird = this.populateDC.populateModalResume("Vazao", data);
      }
    } else {
      this.objMain = [];
      this.toastr.warning("Não foram encontrados dados para o resumo", "Dados Resumo");
    }
  }

  getFullData(data) {
    this.showFullDataTable = false;
    if (data != null) {
      this.objValue = [];
      this.objHeader = [];
      [this.objHeader, this.objValue, this.showTable] = this.populateDC.populateMainTable(data);
      this.showFullDataTable = true;
    } else {
      this.objHeader = [];
      this.objValue = [];
      this.showTable = false;
      this.toastr.warning("Não há dados para exibição", "Dados Tabela Principal");
    }
  }
}