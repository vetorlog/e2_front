import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { ApiService } from 'src/app/services/api.service';
import { EndPoint } from '../../shared/constants/end-point';
import { ObjDataStationModel } from './obj-data-station.model';
import { ObjWeatherModel } from './obj-weather-model';
import { ObjDataEnergyModel } from './obj-params-energy.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'emeter2-land-page',
  templateUrl: './land-page.component.html',
  styleUrls: ['./land-page.component.scss']
})

export class LandPageComponent implements OnInit {
  //INICIO DAS VARIÁVEIS
  siteId;
  dateInitial;
  dateFinal;
  dataSource;
  objParams: ObjDataEnergyModel;
  objWeather: ObjWeatherModel = new ObjWeatherModel();
  objResumHydro = [];
  objDataStation: ObjDataStationModel = new ObjDataStationModel();
  hydroInfo = [];
  showComponent: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.setParams();
    });
  }

  ngOnInit(): void {
    // this.setParams();
  }

  ngDoCheck() {
    // console.log("ngDoCheck")
  }

  navigateCollect() {
    this.router.navigate(['/data/collect', this.siteId])
  }

  postParameters() {
    this.showComponent = false;
    this.dateInitial = '2019-06-01';
    this.dateFinal = '2019-06-06'
    this.dataSource = 'daily'
    this.objParams = undefined
    this.objParams = new ObjDataEnergyModel();
    this.objParams.site_id = this.siteId.toString();
    this.objParams.inicio = this.dateInitial.toString();
    this.objParams.fim = this.dateFinal.toString();
    this.objParams.fonte_dados = this.dataSource.toString()
    this.showComponent = true;

  }

  async setParams() {
    await this.activatedRoute.params.subscribe(params => this.siteId = params.id);
    // this.objParams = undefined
    this.postParameters();
    this.getMeters();
    this.getIdHydroStation()
  }

  getMeters() {
    let params = new HttpParams()
      .set('site_id', this.siteId.toString());

    this.apiService.get(EndPoint.MEDIDORES, params).subscribe(
      data => {
        // this.getDataMeters(data['sitesMeters']);
      },
      err => {
        this.toastr.error("Ocorreu um erro ao buscar os medidores", "Medidores");
      }
    );
  }

  getIdHydroStation() {
    let params = new HttpParams()
      .set('site_id', this.siteId)
    this.apiService.get(EndPoint.DADOS_ESTACAO, params).subscribe(
      data => {
        let keyHydro = Object.keys(data['hydros']);
        //For para preencher o array com os ids e o nome das estações.
        for (var i = 0; i < keyHydro.length; i++) {
          this.hydroInfo.push({
            id: data['hydros'][keyHydro[i]].id,
            name: data['hydros'][keyHydro[i]].name,
            idPoint: this.siteId,
            dateInitial: '2019-05-01 00:00:00',
            dateFinal: '2019-05-02 00:00:00',
          });
        }
      },
      err => {
        this.toastr.error("Ocorreu um erro ao buscar os medidores de hidrologia", "Medidores de Hidrologia");
      }
    );
  }



}
