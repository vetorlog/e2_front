import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataCollectComponent } from './data-collect/data-collect.component';
import { LandPageComponent } from './land-page/land-page.component';
import { SharedModule } from '../shared/shared.module';
import { DataCenterRoutingModule } from './data-center-routing.module';


@NgModule({
  declarations: [
    DataCollectComponent,
    LandPageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DataCenterRoutingModule
  ],
  exports: [
    SharedModule
  ]
})
export class DataCenterModule { }