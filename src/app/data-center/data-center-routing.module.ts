import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandPageComponent } from './land-page/land-page.component';
import { DataCollectComponent } from './data-collect/data-collect.component';


const dataCenterRoutes: Routes = [
    { path: '', component: LandPageComponent},
    { path: 'landing/:id', component: LandPageComponent },
    { path: 'collect/:id', component: DataCollectComponent }
];

@NgModule({
    imports: [RouterModule.forChild(dataCenterRoutes)],
    exports: [RouterModule]
})
export class DataCenterRoutingModule { }