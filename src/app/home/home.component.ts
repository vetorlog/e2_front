import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'emeter2-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  guaranteeValue = '70,155';
  averageValue = '64,154'
  consumptionValue = '51,321'
  generationValue = '44,924'
  objTestNames = [];
  objTest = [];

  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
    this.toastr.info("Olá, bem vindo(a) ao Emeter 2", "BEM VINDO(A)!");

    this.objTestNames.push({
      date: 'Data',
      headerName1: 'Consumo',
      headerName2: 'Geração',
      headerName3: 'Fator de potência',
      headerName4: 'Reativo capacitivo',
      headerName5: 'Reativo indutivo',
      headerName6: 'Geração Fora',
      headerName7: 'Consumo Fora',
    })

    for (var i = 0; i < 10; i++) {
      this.objTest.push({
        date: '01/06/2020',
        value1: '3.568,100',
        value2: '13.243,100',
        value3: '1,000',
        value4: '1.781,951',
        value5: '849,108',
        value6: '1.658,108',
        value7: '711,590',
      })
    }
  }
}