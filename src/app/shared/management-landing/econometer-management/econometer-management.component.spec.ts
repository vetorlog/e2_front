import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EconometerManagementComponent } from './econometer-management.component';

describe('EconometerManagementComponent', () => {
  let component: EconometerManagementComponent;
  let fixture: ComponentFixture<EconometerManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EconometerManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EconometerManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
