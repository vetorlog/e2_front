import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';

@Component({
  selector: 'emeter2-econometer-management',
  templateUrl: './econometer-management.component.html',
  styleUrls: ['./econometer-management.component.scss']
})

export class EconometerManagementComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.initializeChartGauge();
    this.initializeChartBarLine();
  }

  initializeChartGauge() {
    Chart.pluginService.register({
      afterUpdate: function (chart) {
        if (chart.config.options.elements.arc.roundedCornersFor !== undefined) {
          var arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundedCornersFor];
          arc.round = {
            x: (chart.chartArea.left + chart.chartArea.right) / 2,
            y: (chart.chartArea.top + chart.chartArea.bottom) / 2,
            radius: (chart.outerRadius + chart.innerRadius) / 2,
            thickness: (chart.outerRadius - chart.innerRadius) / 1.7 - 1,
            backgroundColor: arc._model.backgroundColor
          }
        }
      },

      afterDraw: function (chart) {
        if (chart.config.options.elements.arc.roundedCornersFor !== undefined) {
          var ctx = chart.chart.ctx;
          var arc = chart.getDatasetMeta(0).data[chart.config.options.elements.arc.roundedCornersFor];
          var startAngle = Math.PI / 2 - arc._view.startAngle;
          var endAngle = Math.PI / 2 - arc._view.endAngle;

          ctx.save();
          ctx.translate(arc.round.x, arc.round.y);
          // console.log(arc.round.startAngle)
          ctx.fillStyle = arc.round.backgroundColor;
          ctx.beginPath();
          ctx.arc(arc.round.radius * Math.sin(startAngle), arc.round.radius * Math.cos(startAngle), arc.round.thickness, 0, 2 * Math.PI);
          ctx.arc(arc.round.radius * Math.sin(endAngle), arc.round.radius * Math.cos(endAngle), arc.round.thickness, 0, 2 * Math.PI);
          ctx.closePath();
          ctx.fill();
          ctx.restore();
        }
      },
    });

    Chart.pluginService.register({
      afterUpdate: function (chart) {
        if (chart.config.options.elements.center) {
          var helpers = Chart.helpers;
          var centerConfig = chart.config.options.elements.center;
          var globalConfig = Chart.defaults.global;
          var ctx = chart.chart.ctx;
          var fontStyle = helpers.getValueOrDefault(centerConfig.fontStyle, globalConfig.defaultFontStyle);
          var fontFamily = helpers.getValueOrDefault(centerConfig.fontFamily, globalConfig.defaultFontFamily);

          if (centerConfig.fontSize)
            var fontSize = centerConfig.fontSize;
          else {
            ctx.save();
            var fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
            var maxFontSize = helpers.getValueOrDefault(centerConfig.maxFontSize, 256);
            var maxText = helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);

            do {
              ctx.font = helpers.fontString(fontSize, fontStyle, fontFamily);
              var textWidth = ctx.measureText(maxText).width;

              if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize)
                fontSize += 1;
              else {
                fontSize -= 1;
                break;
              }
            } while (true)
            ctx.restore();
          }

          chart.center = {
            font: helpers.fontString(fontSize, fontStyle, fontFamily),
            fillStyle: helpers.getValueOrDefault(centerConfig.fontColor, globalConfig.defaultFontColor)
          };
        }
      },

      afterDraw: function (chart) {
        if (chart.center) {
          var centerConfig = chart.config.options.elements.center;
          var ctx = chart.chart.ctx;

          ctx.save();
          ctx.font = chart.center.font;
          ctx.fillStyle = chart.center.fillStyle;
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';

          var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
          var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
          
          ctx.fillText(centerConfig.text, centerX, centerY);
          ctx.restore();
        }
      },
    })

    var config = {
      type: 'doughnut',
      data: {
        labels: [
          "Red",
          "Gray"
        ],
        datasets: [{
          data: [50, 50],
          backgroundColor: [
            "#517DEE",
            "#A3A3A3"
          ],
          hoverBackgroundColor: [
            "#517DEE",
            "#A3A3A3"
          ],
          borderColor: [
            "#517DEE",
            "#A3A3A3"
          ]
        }]
      },
      options: {
        legend: {
          display: false
        },
        elements: {
          arc: {
            roundedCornersFor: 0
          },
          center: {
            maxText: '450,00',
            text: '450,00',
            fontColor: '#517DEE',
            fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
            fontStyle: 'bold',
            minFontSize: 1,
            maxFontSize: 20,
          }
        },
        cutoutPercentage: 80
      }
    };

    var ctx = document.getElementById("chartGauge");
    var myChart = new Chart(ctx, config);

  }

  initializeChartBarLine() {
    var ctx = document.getElementById("chartBarAndLine");
    var mixedChart = new Chart(ctx, {
      type: 'bar',
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        cornerRadius: 20,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              maxTicksLimit: 6
            }
          }],
          xAxes: [{
            gridLines: {
              display: false
            }
          }]
        },
      },
      data: {
        datasets: [{
          label: 'Demanda Máx',
          data: [25, 20, 30, 40, 45, 20, 25, 40, 30],
          backgroundColor: '#517DEE',
          borderColor: '#517DEE',
          barPercentage: 0.4,
          borderWidth: 8,
        }, {
          label: 'Demanda Contrada',
          data: [50, 50, 50, 50, 50, 50, 50, 50, 50],
          fill: false,
          backgroundColor: '#6CC18E',
          borderColor: '#6CC18E',
          type: 'line',
          borderWidth: 8
        }],
        labels: ['01/06/20', '02/06/20', '03/06/20', '04/06/20', '05/06/20', '06/06/20', '07/06/20', '08/06/20', '09/06/20']
      },
    });
  }
}
