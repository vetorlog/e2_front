import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class RequestHydroService {

  constructor(
    private apiService: ApiService,
    private toastr: ToastrService
  ) { }

  async requestHydro(params, endpoint) {
    let dataToReturn = await this.apiService.get(endpoint, params).toPromise().then(
      dataAPI => {
        return dataAPI;
      },
      err => {
        this.toastr.error("Erro ao realizar a requisição: " + endpoint, "Requisição");
        return [];
      });

    return dataToReturn;
  }
}
