export class ObjResumModel {
    name: any
    id: any
    flowAvg: any
    flowMax: any
    flowMin: any
    stageAvg: any
    stageMax: any
    stageMin: any
    rainfall: any
    haveFlow: any
    haveStage: any
    haveRainfall: any
    haveVertimento: any
    weather: any[]
}