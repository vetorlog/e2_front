import { Component, OnInit, Input } from '@angular/core';
import { EndPoint } from '../../constants/end-point';
import { HttpParams } from '@angular/common/http';
import { RequestHydroService } from './requets-hydro.service';
import Chart from 'chart.js';
import { ApiService } from 'src/app/services/api.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'emeter2-hydro-management',
  templateUrl: './hydro-management.component.html',
  styleUrls: ['./hydro-management.component.scss']
})

export class HydroManagementComponent implements OnInit {

  @Input() paramsWeather;
  @Input() paramsResumHydro;
  @Input() paramsGetIdStation;

  constructor(
    private apiService: ApiService,
    private forkJoin: RequestHydroService,
    private toastr: ToastrService
  ) { }

  objResumTable: any = [];
  objWeather: any = [];
  objHydroManagement: any = [];
  objRequestToChart: any = [];
  daysOfWeek: String[] = ["", "SEG", "TER", "QUA", "QUI", "SEX", "SÁB", "DOM"];

  ngOnInit(): void {
    this.getIdHydroStation()
  }

  /**
   * O método é responsável por pegar os ids das estações, que é o parâmetro necessário
   * para qualquer request do componente
   * @author Allan Hissato Maeda
   * 
   */
  getIdHydroStation() {
    for (var i = 0; i < this.paramsGetIdStation.length; i++) {
      this.getRequests(this.paramsGetIdStation[i].id, this.paramsGetIdStation[i].name)
    }
  }

  /**
  * O método é encarregado por fazer 3 requests e, a partir da resposta, montar o objeto final para exibição dos conteúdos no html, incluindo o gráfico.
  * @author Allan Hissato Maeda
  */
  async getRequests(hydroId, nameStation) {
    // Região dos parametros para o request no fork-join
    let paramsResum = new HttpParams()
      .set('site_id', '50')
      .set('hydro_id', hydroId)
      .set('data_inicial', '2019-06-01 00:00:00')
      .set('data_final', '2019-06-06 00:00:00');

    let paramsWeather = new HttpParams()
      .set('estacao_id', hydroId)
      .set('data_referencia', '2019-06-06 00:00:00')

    const dataRequestResum = <any>await this.forkJoin.requestHydro(paramsResum, EndPoint.RESUMO_HIDRO);
    const dataRequestWeather = <any>await this.forkJoin.requestHydro(paramsWeather, EndPoint.PREVISAO_TEMPO);

    this.concatAndFillMainObj(dataRequestResum, dataRequestWeather, nameStation, hydroId);
  }

  concatAndFillMainObj(dataResum, dataWeather, nameStation, hydroId) {
    let arrayWeatherTemp = [];

    this.objHydroManagement.push({
      name: nameStation,
      id: hydroId,
      flowAvg: dataResum['flowAvg'],
      flowMax: dataResum['flowMax'],
      flowMin: dataResum['flowMin'],
      stageAvg: dataResum['stageAvg'],
      stageMax: dataResum['stageMax'],
      stageMin: dataResum['stageMin'],
      rainfall: dataResum['rainfall'],
      haveFlow: dataResum['haveFlow'],
      haveStage: dataResum['haveStage'],
      haveRainfall: dataResum['haveRainfall'],
      haveVertimento: dataResum['haveVertimento'],
    });

    // 'Montagem' do objeto da previsão do tempo
    for (var j = 0; j < dataWeather['previsoes'].length; j++) {
      let dateTimeBr = dataWeather['previsoes'][j].time.substring(0, 10);
      let dateTimeConvertedUs = dateTimeBr.split("/").reverse().join("-");
      let dateTimeUs = new Date(dateTimeConvertedUs);

      arrayWeatherTemp.push({
        icon: dataWeather['previsoes'][j].icon,
        time: dataWeather['previsoes'][j].time,
        precipIntensity: dataWeather['previsoes'][j].precipIntensity,
        day: this.daysOfWeek[dateTimeUs.getDay() + 1]
      });
    }

    // Adicionando o objeto da previsão do tempo, no nosso array temporário
    this.objHydroManagement.forEach(function (element) {
      element.weather = arrayWeatherTemp
    });
    this.objHydroManagement.sort((stationA, stationB) => stationA.name > stationB.name ? 1 : -1)

    // setTimeout(() => {
    //   this.fillObjToChart(dataHydroToChart['dados'], hydroId, nameStation, dataHydroToChart, dataHydroToChart['vertimentos'])
    // }, 1000); //poderia usar desta forma, mas, a tela pode variar seu tempo de 'renderizar' e assim, gerar um erro

    this.getDataToChartHydro(hydroId, nameStation);
  }

  getDataToChartHydro(id, name) {
    let params = new HttpParams()
      .set('site_id', '50')
      .set('hydro_id', id)
      .set('frequency', 'DAILY')
      .set('data_inicial', '2019-06-01 00:00:00')
      .set('data_final', '2019-06-06 00:00:00');

    this.apiService.get(EndPoint.DADOS_HIDRO, params).subscribe(
      data => {
        this.fillObjToChart(data['dados'], id, name, data, data['vertimentos'])
      },
      err => {
        this.toastr.error("Ocorreu um erro ao buscar os dados de hidrologia", "Dados Hidrologia");
        console.log("erro", err);
      }
    );
  }

  /**
    * O método tem como objetivo filtrar os dados do request e montar objetos
    * para serem enviados como parâmetros para inicializar os gráficos de Vazão e Nível
    * @author Allan Hissato Maeda
    */
  fillObjToChart(dataToChart, id, name, dataGeneral, vertimento) {
    var keysFromData = Object.keys(dataToChart);
    let arrDates = [];
    let arrStage = [];
    let arrFlow = [];
    let arrRainfall = [];
    let valorVertimento = []

    // Verificação se há vertimento na estação atual
    if (vertimento != null && vertimento.length > 0) {
      vertimento.forEach((element) => {
        valorVertimento.push((element.vertimento))
      });
    }

    // Montando os objetos para serem exibidos no gráfico
    for (var i = 0; i < keysFromData.length; i++) {
      arrDates.push(dataToChart[keysFromData[i]].date)
      arrStage.push(dataToChart[keysFromData[i]].stageAvg)
      arrFlow.push(dataToChart[keysFromData[i]].flowAvg)
      // arrRainfall.push(dataToChart[keysFromData[i]].rainfall) caso haja necessidade das informações de chuva no periodo de datas

    }

    // Verificação para a chamada do gráfico, se existe(m) nível e/ou vazão. 
    if (dataGeneral.temNivel == true) {
      if (valorVertimento.length > 0) {
        this.initializeGenerationStage(arrStage, id, name, arrDates, vertimento)
      } else {
        this.initializeGenerationStage(arrStage, id, name, arrDates, [])
      }
    }
    if (dataGeneral.temVazao == true) {
      this.initializeGenerationFlow(arrFlow, id, name, arrDates)
    }

  }

  /**
    * Método para inicializar o gráfico de Nível, com ou sem vertimento.
    * @author Allan Hissato Maeda
    */
  initializeGenerationStage(obj, id, name, arrDates, vertimento) {
    // Se a estação tiver vertimento, será montado um gráfico com dois tipos:
    // um de Nível e outro de Vertimento
    if (vertimento.length > 0) {
      let valuesVertimento = vertimento.map(res => res.vertimento)
      var ctx = document.getElementById(`myChartStage${name + id}`);
      var myChart = new Chart(ctx, {
        type: 'bar',

        data: {
          datasets: [
            {
              label: 'Nível',
              data: obj,
              backgroundColor: '#517DEE',
              type: 'bar',
              order: 2
              // this dataset is drawn below
            },
            {
              label: 'Vertimento',
              type: 'line',
              data: valuesVertimento,
              backgroundColor: '#cf6d17',
              borderColor: '#cf6d17',
              fill: false
            }],
          labels: arrDates,
          responsive: true,
          // maintainAspectRatio:true,
        },
        options: {
          responsive: true,
          // maintainAspectRatio:true,
          scales: {
            yAxes: [{
              ticks: {
                // beginAtZero: true,
                callback: function (label) {
                  return transform(label);
                }
              }
            }],
            xAxes: [{
              gridLines: {
                display: false
              },
              barPercentage: 0.5
            }]
          },
          tooltips: {
            // enabled: true,
            callbacks: {
              label: function (tooltipItem) {
                return transform(tooltipItem.value);
              }
            }
          },
        },
      });
    } else {
      var ctx = document.getElementById(`myChartStage${name + id}`);
      var myChart = new Chart(ctx, {
        type: 'bar',
        options: {
          responsive: true,
          // maintainAspectRatio:true,
          scales: {
            yAxes: [{
              ticks: {
                // beginAtZero: true,
                callback: function (label) {
                  return transform(label);
                }
              }
            }],
            xAxes: [{
              gridLines: {
                display: false
              }
            }]
          },
          tooltips: {
            // enabled: true,
            callbacks: {
              label: function (tooltipItem) {
                return transform(tooltipItem.value);
              }
            }
          }
        },
        data: {
          datasets: [{
            label: 'Nível',
            data: obj,
            backgroundColor: '#517DEE',
            type: 'bar',
            order: 2,
            barPercentage: 0.5
            // this dataset is drawn below
          }],
          labels: arrDates,
          responsive: true,
          // maintainAspectRatio:true,
        }
      });
    }
  }

  /**
    * Método para inicializar o gráfico de Vazão.
    * @author Allan Hissato Maeda
    */
  initializeGenerationFlow(obj, id, name: string, arrDates) {
    // Inicialização do gráfico de Vazão
    var ctx = document.getElementById(`myChartFlow${name + id}`);
    var myChart = new Chart(ctx, {
      type: 'bar',
      options: {
        responsive: true,
        // maintainAspectRatio:true,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              callback: function (label) {
                return transform(label);
              }
            }
          }],
          xAxes: [{
            gridLines: {
              display: false
            },

          }]
        },
        tooltips: {
          // enabled: true,
          callbacks: {
            label: function (tooltipItem) {
              return transform(tooltipItem.value);
            }
          }
        }
      },
      data: {
        datasets: [{
          label: 'Vazão',
          data: obj,
          backgroundColor: '#6CC18E',
          type: 'bar',
          order: 2,
          barPercentage: 0.5
          // this dataset is drawn below
        }],
        labels: arrDates,
        responsive: true,
        // maintainAspectRatio:true,
      }
    });
  }
}

function transform(value) {
  if (value === null || value === "null") {
    return " - ";
  } else if (value) {
    let parseValue = parseFloat(value);

    if (isNaN(parseValue)) {
      return " - "
    } else {
      return (parseValue.toLocaleString('pt-BR', { minimumFractionDigits: 3 }))
    }
  } else {
    return value
  }
}
