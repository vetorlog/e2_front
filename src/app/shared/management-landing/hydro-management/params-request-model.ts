export class ParamsRequestModel {
    site_id: string;
    estacao_id: string;
    hydroId: HydroId[];
    data_inical: string;
    data_final: string;
    data_referencia: string;
    endpoint: string;
    name: string;
    
}
export class HydroId {
    id: string;
    name: string;
}