import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HydroManagementComponent } from './hydro-management.component';

describe('HydroManagementComponent', () => {
  let component: HydroManagementComponent;
  let fixture: ComponentFixture<HydroManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HydroManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HydroManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
