import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerationManagementComponent } from './generation-management.component';

describe('GenerationManagementComponent', () => {
  let component: GenerationManagementComponent;
  let fixture: ComponentFixture<GenerationManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerationManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerationManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
