import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { EndPoint } from '../../constants/end-point';
import { HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import Chart from 'chart.js';
import { ValueMaskPipe } from '../../pipe/pipe';

@Component({
  selector: 'emeter2-generation-management',
  templateUrl: './generation-management.component.html',
  styleUrls: ['./generation-management.component.scss']
})
export class GenerationManagementComponent implements OnInit {
  @Input() params;

  constructor(private apiService: ApiService, private toast: ToastrService, private valueMask: ValueMaskPipe) { }

  gaugeData = {
    mediaGerada: 0,
    garantiaFisica: 0,
  };
  dataConsumnGeneration = {
    consumo: "0",
    geracao: "0",
  }
  chartsGeneration = [];

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.toast.info("Buscando os dados...", "Aguarde");
    this.getDataGenerationGraph();
  }

  /**
   * O método é responsável por buscar os dados da API, popular o objeto com parâmetros necessários
   * e em seguida, chamar a função para inicializar e exibir o gráfico.
   * @author Allan Hissato Maeda
   * 
   */
  async getDataGenerationGraph() {
    //Request da API para trazer os dados e fazer possíveis tratamento para serem exibidos no html
    let dataChartGenerationConsummer = await this.apiService.get(EndPoint.DADOS_MEDICAO, this.params).toPromise().then(
      data => {
        if (data != null) {
          //Obter os valores da resposta do request
          let objectValueData = JSON.stringify(Object.values(data['dataMetersIntegralizadas']));

          //Convertendo para poder trabalhar com as propriedades desejadas para montar o objeto final
          let objDataMeters = JSON.parse(objectValueData)
          let tempDates = [];
          let tempGenValues = [];
          let tempConValues = [];
          //'For' responsável por popular arrays temporários contendo os valores para montar o objeto
          // e serem enviados por parametro ao método que irá inicializar o gráfico
          for (var i = 0; i < objDataMeters.length; i++) {
            tempDates.push(objDataMeters[i].dataColeta.substring(0, 10));
            tempGenValues.push(parseFloat(objDataMeters[i].geracao));
            tempConValues.push(parseFloat(objDataMeters[i].consumo));
          }

          //Valores das somas totais que vão ser exibidos na legenda do cabeçalho do card-generation
          this.dataConsumnGeneration.geracao = this.valueMask.transform(tempGenValues.reduce((a, b) => a + b))
          this.dataConsumnGeneration.consumo = transform(tempConValues.reduce((a, b) => a + b))

          //Objeto final, contendo os valores necessários a serem apresentados no html
          //e também para inicializar o gráfico.
          return {
            date: tempDates,
            consumn: tempConValues,
            generation: tempGenValues
          }
        }
        else {
          this.toast.warning("Não foram encontrados dados para gerar os gráficos", "Sem Dados")
          this.cleanDataChart();
          return {};
        }
      }
    );

    let dataGauge = await this.getGenerationGauge().toPromise().then(
      data => {
        return data;
      },
      err => {
        this.toast.error("Ocorreu um erro para buscar os dados do gauge", "Erro");
        console.log('erro', err);
      }
    )

    this.initializeGenerationGraph(dataChartGenerationConsummer)
    this.initializeGuaranteeGraph(dataGauge);
  }

  cleanDataChart() {
    this.gaugeData.mediaGerada = 0;
    this.gaugeData.garantiaFisica = 0;
    for (var i = 0; i < this.chartsGeneration.length; i++) {
      this.chartsGeneration[i].destroy();
    }
  }

  /**
     * O método é responsável por buscar os dados da API, em seguida é chamado o método para fazer o gráfico da Garantia Física (Gauge), sendo enviado como parâmetro a resposta do request.
     * @author Allan Hissato Maeda
     * 
     */
  getGenerationGauge() {
    let params = new HttpParams()
      .set('site_id', this.params.site_id)
      .set('inicio', this.params.inicio + '00:00:00')
      .set('fim', this.params.fim + '00:00:00');

    return this.apiService.get(EndPoint.GAUGE_GERACAO, params);
  }

  /**
  * Método responsável por inicializar/exibir o gráfico
  * @author Allan Hissato Maeda
  * 
  * @params: date - array de datas do periodo dos dias que serão exibidos no gráfico.
  *          consumn - array de valores de consumo no periodo dos dias.
  *          generation - array de valores da geração no periodo dos dias.
  */
  initializeGenerationGraph(dataChart) {
    //Configurações do chart.js para montar e exibir os gráficos.
    var ctx = document.getElementById('myChart');
    this.chartsGeneration.push(new Chart(ctx, {
      type: 'bar',
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: false,
              callback: function (label) {
                return transform(label);
              }
            },
          }],
          xAxes: [{
            gridLines: {
              display: false
            }
          }]
        },
        tooltips: {
          enabled: true,
          callbacks: {
            label: function (tooltipItem) {
              return transform(tooltipItem.value) + ' kWh';
            }
          }
        }
      },
      data: {
        datasets: [{
          label: 'Consumo',
          data: dataChart.consumn,
          backgroundColor: '#517DEE',
          type: 'bar',
          order: 2
        }, {
          label: 'Geração',
          data: dataChart.generation,
          backgroundColor: '#6CC18E',
          type: 'bar',
          order: 1
        }],
        labels: dataChart.date
      }
    }));
  }

  /**
     * Método responsável por inicializar/exibir o gráfico
     * @author Allan Hissato Maeda
     * @params: dataToChart - array de dados oriundos do request para montar o gráfico.
  */
  initializeGuaranteeGraph(dataToChart) {
    let text;
    this.gaugeData.mediaGerada = dataToChart.geracaoMedia;
    this.gaugeData.garantiaFisica = dataToChart.inicioAmarelo;
    if (dataToChart.geracaoMedia != null && dataToChart.fimAmarelo > 0) {
      text = (parseFloat(dataToChart.geracaoMedia) / parseFloat(dataToChart.fimAmarelo) * 100).toFixed(2) + '% de ' + parseFloat(dataToChart.fimAmarelo) + ' MW';
    } else {
      text = '0 MW';
    }
    var color = '#DF5353';
    if (dataToChart.geracaoMedia != null && dataToChart.fimAmarelo > 0) {
      if (parseFloat(dataToChart.geracaoMedia) >= parseFloat(dataToChart.fimAmarelo)) {
        color = '#55BF3B';
      }
    }

    var ctx = document.getElementById('guaranteeChart')
    this.chartsGeneration.push(new Chart(ctx, {
      type: 'doughnut',
      data: {
        datasets: [{
          backgroundColor: [color],
          data: [parseFloat(dataToChart.geracaoMedia)],
        },
        ],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
          display: true,
          text: text,
          position: 'top',
          fontSize: 16,
          fontStyle: 'bold',
        },
        circumference: 1 * Math.PI,
        rotation: 1 * Math.PI,
        cutoutPercentage: 60,
        events: [],
        arrowColor: "#444",
        showMarkers: true,
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
      },
    }));
  }
}

function transform(value) {
  if (value === null || value === "null") {
    return " - ";
  } else if (value) {
    let parseValue = parseFloat(value);

    if (isNaN(parseValue)) {
      return " - "
    } else {
      return (parseValue.toLocaleString('pt-BR', { minimumFractionDigits: 3 }))
    }
  } else {
    return value
  }
}