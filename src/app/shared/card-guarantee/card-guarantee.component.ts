import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'emeter2-card-guarantee',
  templateUrl: './card-guarantee.component.html',
  styleUrls: ['./card-guarantee.component.scss']
})
export class CardGuaranteeComponent implements OnInit {
  @Input() guarantee;
  @Input() averageGenerated;
  constructor() { }

  ngOnInit(): void {
  }

}
