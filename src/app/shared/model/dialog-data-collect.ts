


export class DialogDataCollectModel {
    public title: string;
    details: details[] = [];
}

class details {
    title: string;
    value: string;
}