import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'emeter2-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent implements OnInit {
  // types: xls, pdf, add, update, search,
  @Input() type: string;

  constructor() { }

  ngOnInit(): void {
  }

}