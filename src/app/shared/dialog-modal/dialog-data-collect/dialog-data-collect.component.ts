import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogDataCollectModel } from '../../model/dialog-data-collect';

@Component({
  selector: 'emeter2-dialog-data-collect',
  templateUrl: './dialog-data-collect.component.html',
  styleUrls: ['./dialog-data-collect.component.scss']
})
export class DialogDataCollectComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogObj: DialogDataCollectModel
  ) { }

  ngOnInit(): void {
  }

}
