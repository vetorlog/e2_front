// Esta classe deverá conter todos os metódos que sejam práticos e úteis
// para serem usados em todo o projeto

export class Util {

    /**
   * O metódo irá altenar a ordem das datas, deixando no padrão da api.
   * Ex: 15/01/2020 se transformará em 2020-01-15
   * @author Wagner Castilho
   *
   * @params date - data para ser convertida para o novo padrão
   * @returns yyyy-MM-dd
   */
    convertDateToSendApi(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    /**
   * The function return a first and actual day of the month
   * @author Wagner Castilho
   *
   */
    getInitEndDateTime() {
        var myDate = new Date();
        var d = myDate.getDate();
        var m = myDate.getMonth() + 1;
        var y = myDate.getFullYear();

        if (m < 10 && d < 10) {
            var dateTimeInit = '0' + m + '-01-' + y;
            var dateTimeEnd = '0' + m + '-' + '0' + d + '-' + y;
        } else if (m < 10) {
            var dateTimeInit = '0' + m + '-01-' + y;
            var dateTimeEnd = + '0' + m + '-' + d + '-' + y;
        } else if (d < 10) {
            var dateTimeInit = m + '-01-' + y;
            var dateTimeEnd = m + '-' + '0' + d + '-' + y;
        } else {
            var dateTimeInit = m + '-01-' + y;
            var dateTimeEnd = m + '-' + d + '-' + y;
        }

        return { dateTimeInit, dateTimeEnd };
    }

    /**
    * The method check if exist a custom variable
    * @author Wagner Camargo Castilho
    *
    */
    checkVariablesCustom(objHydro) {
        switch (true) {
            case (objHydro.a1 != null && objHydro.a1 > "2"):
                return true;
            case (objHydro.a2 != null && objHydro.a2 > "2"):
                return true;
            case (objHydro.a3 != null && objHydro.a3 > "2"):
                return true;
            case (objHydro.a4 != null && objHydro.a4 > "2"):
                return true;
            case (objHydro.a5 != null && objHydro.a5 > "2"):
                return true;
            case (objHydro.a6 != null && objHydro.a6 > "2"):
                return true;
            case (objHydro.a7 != null && objHydro.a7 > "2"):
                return true;
            case (objHydro.a8 != null && objHydro.a8 > "2"):
                return true;
            case (objHydro.d1 != null && objHydro.d1 > "2"):
                return true;
            case (objHydro.d2 != null && objHydro.d2 > "2"):
                return true;
            case (objHydro.d3 != null && objHydro.d3 > "2"):
                return true;
            case (objHydro.d4 != null && objHydro.d4 > "2"):
                return true;
            default:
                return false;
        }
    }

    /**
   * The method get a interval according to what was selected in screen
   * @author Wagner Castilho
   *
   * @params valueSelectedInterval - contains the interval who was selected in screen
   */
    getIntervalAndDataSource(valueSelectedInterval: any) {
        var interval: string, dataSource: string;
        if (valueSelectedInterval == "5min") {
            interval = "5";
            dataSource = "";
            return { interval, dataSource };
        }
        if (valueSelectedInterval == "15min") {
            interval = "15";
            dataSource = "quarter";
            return { interval, dataSource };
        }
        else if (valueSelectedInterval == "1hora") {
            interval = "horario";
            dataSource = "hourly";
            return { interval, dataSource };
        }
        else if (valueSelectedInterval == "1dia") {
            interval = "diario";
            dataSource = "daily";
            return { interval, dataSource };
        }
        else if (valueSelectedInterval == "1mes") {
            interval = "mensal";
            dataSource = "monthly";
            return { interval, dataSource };
        }
    }
}