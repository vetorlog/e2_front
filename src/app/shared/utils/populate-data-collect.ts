import { ValueMaskPipe, DateWithoutSecondMaskPipe } from '../pipe/pipe';
import { DialogDataCollectModel } from '../model/dialog-data-collect';

export class PopulateDataCollectUtil {
    private valueMask: ValueMaskPipe = new ValueMaskPipe();
    private dateMask: DateWithoutSecondMaskPipe = new DateWithoutSecondMaskPipe();

    populateResume(typeResume, objectResum) {
        if (typeResume == "Energia") {
            return {
                titleText: "Energia",
                subtitle1: "Geração",
                subtitle2: "Consumo",
                value1: Number(objectResum.geracao) > 1000 ? Number(objectResum.geracao) / 1000 : Number(objectResum.geracao),
                value2: Number(objectResum.consumo) > 1000 ? Number(objectResum.consumo) / 1000 : Number(objectResum.consumo),
                typeCard: "energy"
            }
        }
        else if (typeResume == "Reativo") {
            return {
                titleText: "Reativo",
                subtitle1: "DMCR",
                subtitle2: "UFER",
                value1: objectResum.dmcr,
                value2: objectResum.ufer,
                typeCard: "energy"
            }
        }
        else if (typeResume == "Demanda") {
            return {
                titleText: "Demanda",
                subtitle1: "Máx. Ponta",
                subtitle2: "Máx. Fora",
                value1: this.obtainGreatestValue(objectResum.demandaMaximaConsumoPonta, objectResum.demandaMaximaGeracaoPonta),
                value2: this.obtainGreatestValue(objectResum.demandaMaximaConsumoFora, objectResum.demandaMaximaGeracaoFora),
                typeCard: "energy"
            }
        }
        else if (typeResume == "Energia SCDE") {
            return {
                titleText: "Energia SCDE",
                subtitle1: "Geração",
                subtitle2: "Consumo",
                value1: Number(objectResum.geracao) > 1000 ? Number(objectResum.geracao) / 1000 : Number(objectResum.geracao),
                value2: Number(objectResum.consumo) > 1000 ? Number(objectResum.consumo) / 1000 : Number(objectResum.consumo),
                typeCard: "energy"
            }
        }
        else if (typeResume == "Reativo SCDE") {
            return {
                titleText: "Reativo SCDE",
                subtitle1: "DMCR",
                subtitle2: "UFER",
                value1: objectResum.dmcr,
                value2: objectResum.ufer,
                typeCard: "energy"
            }
        }
        else if (typeResume == "Demanda SCDE") {
            return {
                titleText: "Demanda SCDE",
                subtitle1: "Máx. Ponta",
                subtitle2: "Máx. Fora",
                value1: this.obtainGreatestValue(objectResum.demandaMaximaConsumoPonta, objectResum.demandaMaximaGeracaoPonta),
                value2: this.obtainGreatestValue(objectResum.demandaMaximaConsumoFora, objectResum.demandaMaximaGeracaoFora),
                typeCard: "energy"
            }
        }
        else if (typeResume == "Chuva") {
            return {
                titleText: "Chuva",
                subtitle1: "Total",
                subtitle2: "null",
                subtitle3: "null",
                value1: objectResum.rainfall != null || objectResum.rainfall != "null" ? this.valueMask.transform(objectResum.rainfall) : "0",
                value2: "null",
                value3: "null",
                typeCard: "hydro"
            }
        }
        else if (typeResume == "Nivel") {
            return {
                titleText: "Nível",
                subtitle1: "Mín.",
                subtitle2: "Méd.",
                subtitle3: "Máx.",
                value1: objectResum.stageMin != null || objectResum.stageMin != "null" ? this.valueMask.transform(objectResum.stageMin) : "0",
                value2: objectResum.stageAvg != null || objectResum.stageAvg != "null" ? this.valueMask.transform(objectResum.stageAvg) : "0",
                value3: objectResum.stageMax != null || objectResum.stageMax != "null" ? this.valueMask.transform(objectResum.stageMax) : "0",
                typeCard: "hydro"
            }
        }
        else if (typeResume == "Vazao") {
            return {
                titleText: "Vazão",
                subtitle1: "Mín.",
                subtitle2: "Méd.",
                subtitle3: "Máx.",
                value1: objectResum.flowMin != null || objectResum.flowMin != "null" ? this.valueMask.transform(objectResum.flowMin) : "0",
                value2: objectResum.flowAvg != null || objectResum.flowAvg != "null" ? this.valueMask.transform(objectResum.flowAvg) : "0",
                value3: objectResum.flowMax != null || objectResum.flowMax != "null" ? this.valueMask.transform(objectResum.flowMax) : "0",
                typeCard: "hydro"
            }
        }
    }

    populateMainTable(data) {
        let objValue: PeriodicElement[] = [];
        let objHeader: any;

        if (data.typeMeter == "principal_retaguarda") {
            let valueObj = Object.values(data.dataMeters);
            if (data.intervalSelected == "5min" || data.intervalSelected == "15min") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: valueObj[i]['dataColeta'],
                        value2: this.valueMask.transform(valueObj[i]['geracao']) + ' kWh',
                        value3: this.valueMask.transform(valueObj[i]['consumo']) + ' kWh',
                        value4: this.valueMask.transform(valueObj[i]['kvarhIndutivoGeracao'] + valueObj[i]['kvarhCapacitivoConsumo']) + " kVArh",
                        value5: this.valueMask.transform(valueObj[i]['kvarhIndutivoConsumo'] + valueObj[i]['kvarhCapacitivoGeracao']) + " kVArh",
                        value6: this.valueMask.transform(valueObj[i]['valorTensaoA']) + ' kV',
                        value7: this.valueMask.transform(valueObj[i]['valorTensaoB']) + ' kV',
                        value8: this.valueMask.transform(valueObj[i]['valorTensaoC']) + ' kV',
                        value9: this.valueMask.transform(valueObj[i]['valorCorrenteA']) + ' A',
                        value10: this.valueMask.transform(valueObj[i]['valorCorrenteB']) + ' A',
                        value11: this.valueMask.transform(valueObj[i]['valorCorrenteC']) + ' A',
                        value12: valueObj[i]['sh']
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Geração",
                    'value3': "Consumo",
                    'value4': "Reativa Capacitiva",
                    'value5': "Reativa Indutiva",
                    'value6': "Tensão A",
                    'value7': "Tensão B",
                    'value8': "Tensão C",
                    'value9': "Corrente A",
                    'value10': "Corrente B",
                    'value11': "Corrente C",
                    'value12': "Posto Horário"
                };
                return [objHeader, objValue, true];
            }
            else if (data.intervalSelected == "1hora") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: valueObj[i]['dataColeta'],
                        value2: this.valueMask.transform(valueObj[i]['geracao']) + ' kWh',
                        value3: this.valueMask.transform(valueObj[i]['consumo']) + ' kWh',
                        value4: this.valueMask.transform(this.obterFatorCargaPontaFora(valueObj[i])) + ' %',
                        value5: this.valueMask.transform(valueObj[i]['kvarhIndutivoGeracao'] + valueObj[i]['kvarhCapacitivoConsumo']) + " kVArh",
                        value6: this.valueMask.transform(valueObj[i]['kvarhIndutivoConsumo'] + valueObj[i]['kvarhCapacitivoGeracao']) + " kVArh",
                        value7: this.valueMask.transform(valueObj[i]['fatorPotencia']),
                        value8: this.valueMask.transform(valueObj[i]['dmcr']) + ' kWh',
                        value9: this.valueMask.transform(valueObj[i]['ufer']) + ' kWh',
                        value10: this.valueMask.transform(valueObj[i]['valorTensaoA']) + ' kV',
                        value11: this.valueMask.transform(valueObj[i]['valorTensaoB']) + ' kV',
                        value12: this.valueMask.transform(valueObj[i]['valorTensaoC']) + ' kV',
                        value13: this.valueMask.transform(valueObj[i]['valorCorrenteA']) + ' A',
                        value14: this.valueMask.transform(valueObj[i]['valorCorrenteB']) + ' A',
                        value15: this.valueMask.transform(valueObj[i]['valorCorrenteC']) + ' A',
                        value16: valueObj[i]['sh']
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Geração",
                    'value3': "Consumo",
                    'value4': "Fator de Carga",
                    'value5': "Reativa Capacitiva",
                    'value6': "Reativa Indutiva",
                    'value7': "Fator de Potência",
                    'value8': "DMCR",
                    'value9': "UFER",
                    'value10': "Tensão A",
                    'value11': "Tensão B",
                    'value12': "Tensão C",
                    'value13': "Corrente A",
                    'value14': "Corrente B",
                    'value15': "Corrente C",
                    'value16': "Posto Horário"
                };
                return [objHeader, objValue, true];
            }
            else if (data.intervalSelected == "1dia") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: this.valueMask.transform(valueObj[i]['dataColeta']),
                        value2: this.valueMask.transform(valueObj[i]['geracao']) + ' kWh',
                        value3: this.valueMask.transform(valueObj[i]['consumo']) + ' kWh',
                        value4: this.valueMask.transform(valueObj[i]['valorTensaoA']) + ' kV',
                        value5: this.valueMask.transform(valueObj[i]['valorTensaoB']) + ' kV',
                        value6: this.valueMask.transform(valueObj[i]['valorTensaoC']) + ' kV',
                        value7: this.valueMask.transform(valueObj[i]['valorCorrenteA']) + ' A',
                        value8: this.valueMask.transform(valueObj[i]['valorCorrenteB']) + ' A',
                        value9: this.valueMask.transform(valueObj[i]['valorCorrenteC']) + ' A'
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Geração",
                    'value3': "Consumo",
                    'value4': "Tensão A",
                    'value5': "Tensão B",
                    'value6': "Tensão C",
                    'value7': "Corrente A",
                    'value8': "Corrente B",
                    'value9': "Corrente C",
                };
                return [objHeader, objValue, true];
            }
            else if (data.intervalSelected == "1mes") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: valueObj[i]['dataColeta'],
                        value2: this.valueMask.transform(valueObj[i]['geracao']) + ' kWh',
                        value3: this.valueMask.transform(valueObj[i]['consumo']) + ' kWh',
                        value4: this.valueMask.transform(valueObj[i]['estimativaContabilizacao']) + ' kWh',
                        value5: this.valueMask.transform(valueObj[i]['valorTensaoA']) + ' kV',
                        value6: this.valueMask.transform(valueObj[i]['valorTensaoB']) + ' kV',
                        value7: this.valueMask.transform(valueObj[i]['valorTensaoC']) + ' kV',
                        value8: this.valueMask.transform(valueObj[i]['valorCorrenteA']) + ' A',
                        value9: this.valueMask.transform(valueObj[i]['valorCorrenteB']) + ' A',
                        value10: this.valueMask.transform(valueObj[i]['valorCorrenteC']) + ' A'
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Geração",
                    'value3': "Consumo",
                    'value5': "Estimativa de Contabilização",
                    'value6': "Tensão A",
                    'value7': "Tensão B",
                    'value8': "Tensão C",
                    'value9': "Corrente A",
                    'value0': "Corrente B",
                    'value10': "Corrente C",
                };
                return [objHeader, objValue, true];
            }
        }
        else if (data.typeMeter == "consistidas") {
            let valueObj = Object.values(data.dataMetersIntegralizadas);
            if (data.intervalSelected == "15min") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: valueObj[i]['dataColeta'],
                        value2: this.valueMask.transform(valueObj[i]['geracao'] == "null" ? "-" : valueObj[i]['geracao']) + ' kWh',
                        value3: this.valueMask.transform(valueObj[i]['consumo'] == "null" ? "-" : valueObj[i]['consumo']) + ' kWh',
                        value4: this.valueMask.transform(valueObj[i]['kvarhIndutivoGeracao'] + valueObj[i]['kvarhCapacitivoConsumo']) + " kVArh",
                        value5: this.valueMask.transform(valueObj[i]['kvarhIndutivoConsumo'] + valueObj[i]['kvarhCapacitivoGeracao']) + " kVArh",
                        value6: valueObj[i]['sh'],
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Geração",
                    'value3': "Consumo",
                    'value4': "Reativa Capacitiva",
                    'value5': "Reativa Indutiva",
                    'value6': "Posto Horário"
                };

                return [objHeader, objValue, true];
            }
            else if (data.intervalSelected == "1hora") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push(
                        {
                            value1: valueObj[i]['dataColeta'],
                            value2: this.valueMask.transform(valueObj[i]['geracao'] == "null" ? "-" : valueObj[i]['geracao']) + ' kWh',
                            value3: this.valueMask.transform(valueObj[i]['consumo'] == "null" ? "-" : valueObj[i]['consumo']) + ' kWh',
                            value4: this.valueMask.transform(valueObj[i]['fatorPotencia']),
                            value5: this.valueMask.transform(valueObj[i]['geracaoPonta'] == "null" ? "-" : valueObj[i]['geracaoPonta']) + ' kWh',
                            value6: this.valueMask.transform(valueObj[i]['geracaoFora'] == "null" ? "-" : valueObj[i]['geracaoFora']) + ' kWh',
                            value7: this.valueMask.transform(valueObj[i]['consumoPonta'] == "null" ? "-" : valueObj[i]['consumoPonta']) + ' kWh',
                            value8: this.valueMask.transform(valueObj[i]['consumoFora'] == "null" ? "-" : valueObj[i]['consumoFora']) + ' kWh',
                            value9: this.valueMask.transform(valueObj[i]['kvarhIndutivoGeracao'] + valueObj[i]['kvarhCapacitivoConsumo']) + " kVArh",
                            value10: this.valueMask.transform(valueObj[i]['kvarhIndutivoConsumo'] + valueObj[i]['kvarhCapacitivoGeracao']) + " kVArh",
                            demandInValue: this.valueMask.transform(this.obtainGreatestValue(valueObj[i]['demandaMaximaConsumoPonta'], valueObj[i]['demandaMaximaGeracaoPonta'])) + ' KW',
                            demandOutValue: this.valueMask.transform(this.obtainGreatestValue(valueObj[i]['demandaMaximaConsumoFora'], valueObj[i]['demandaMaximaGeracaoFora'])) + ' KW',
                            timeDemandInValue: this.obtainGreatestValue(valueObj[i]['timeDemandaMaximaConsumoPonta'], valueObj[i]['timeDemandaMaximaGeracaoPonta']),
                            timeDemandOutValue: this.obtainGreatestValue(valueObj[i]['timeDemandaMaximaConsumoFora'], valueObj[i]['timeDemandaMaximaGeracaoFora'])
                        })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Geração",
                    'value3': "Consumo",
                    'value4': "Fator de Potência",
                    'value5': "Geração Ponta",
                    'value6': "Geração Fora",
                    'value7': "Consumo Ponta",
                    'value8': "Consumo Fora",
                    'value9': "Reativa Capacitiva",
                    'value10': "Reativa Indutiva",
                    'demandInValue': "Demanda Máx. Ponta",
                    'demandOutValue': "Demanda Máx. Fora",
                };

                return [objHeader, objValue, true];
            }
            else if (data.intervalSelected == "1dia" || data.intervalSelected == "1mes") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: valueObj[i]['dataColeta'],
                        value2: this.valueMask.transform(valueObj[i]['geracao'] == "null" ? "-" : valueObj[i]['geracao']) + ' kWh',
                        value3: this.valueMask.transform(valueObj[i]['consumo'] == "null" ? "-" : valueObj[i]['consumo']) + ' kWh',
                        value4: this.valueMask.transform(valueObj[i]['geracaoPonta'] == "null" ? "-" : valueObj[i]['geracaoPonta']) + ' kWh',
                        value5: this.valueMask.transform(valueObj[i]['geracaoFora'] == "null" ? "-" : valueObj[i]['geracaoFora']) + ' kWh',
                        value6: this.valueMask.transform(valueObj[i]['consumoPonta'] == "null" ? "-" : valueObj[i]['consumoPonta']) + ' kWh',
                        value7: this.valueMask.transform(valueObj[i]['consumoFora'] == "null" ? "-" : valueObj[i]['consumoFora']) + ' kWh',
                        demandInValue: this.valueMask.transform(this.obtainGreatestValue(valueObj[i]['demandaMaximaConsumoPonta'], valueObj[i]['demandaMaximaGeracaoPonta'])) + ' KW',
                        demandOutValue: this.valueMask.transform(this.obtainGreatestValue(valueObj[i]['demandaMaximaConsumoFora'], valueObj[i]['demandaMaximaGeracaoFora'])) + ' KW',
                        timeDemandInValue: this.obtainGreatestValue(valueObj[i]['timeDemandaMaximaConsumoPonta'], valueObj[i]['timeDemandaMaximaGeracaoPonta']),
                        timeDemandOutValue: this.obtainGreatestValue(valueObj[i]['timeDemandaMaximaConsumoFora'], valueObj[i]['timeDemandaMaximaGeracaoFora']),
                        value10: this.valueMask.transform(valueObj[i]['kvarhIndutivoGeracao'] + valueObj[i]['kvarhCapacitivoConsumo']) + " kVArh",
                        value11: this.valueMask.transform(valueObj[i]['kvarhIndutivoConsumo'] + valueObj[i]['kvarhCapacitivoGeracao']) + " kVArh"
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Geração",
                    'value3': "Consumo",
                    'value4': "Geração Ponta",
                    'value5': "Geração Fora",
                    'value6': "Consumo Ponta",
                    'value7': "Consumo Fora",
                    'demandInValue': "Demanda Ponta",
                    'demandOutValue': "Demanda Fora",
                    'value10': "Reativa Capacitiva",
                    'value11': "Reativa Indutiva",
                };

                return [objHeader, objValue, true];
            }
        }
        else if (data.typeMeter.includes("scde")) {
            let valueObj = data.typeMeter == "scde_consolidado" ? Object.values(data.dataScde) : Object.values(data.dataScdeOrigem);
            if (data.intervalSelected == "1hora" && data.typeMeter == "scde_consolidado") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: this.dateMask.transform(valueObj[i]['dataHora']),
                        value2: valueObj[i]['tipoEnergia'] == "null" || valueObj[i]['notificacaoColeta'] == "" ? "-" : valueObj[i]['tipoEnergia'],
                        value3: this.valueMask.transform(valueObj[i]['geracaoAtiva'] == "null" ? "-" : valueObj[i]['geracaoAtiva']) + ' kWh',
                        value4: this.valueMask.transform(valueObj[i]['geracaoReativa'] == "null" ? "-" : valueObj[i]['geracaoReativa']) + ' kVArh',
                        value5: this.valueMask.transform(valueObj[i]['consumoAtivo'] == "null" ? "-" : valueObj[i]['consumoAtivo']) + ' kWh',
                        value6: this.valueMask.transform(valueObj[i]['consumoReativo'] == "null" ? "-" : valueObj[i]['consumoReativo']) + ' kVArh',
                        value7: valueObj[i]['intervalosFaltantes'] == "null" || valueObj[i]['notificacaoColeta'] == "" ? "-" : valueObj[i]['intervalosFaltantes'],
                        value8: valueObj[i]['status'] == "null" || valueObj[i]['notificacaoColeta'] == "" ? "-" : valueObj[i]['status'],
                        value9: valueObj[i]['motivo'] == "null" || valueObj[i]['notificacaoColeta'] == "" ? "-" : valueObj[i]['motivo']
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Tipo Energia",
                    'value3': "Geração Ativa",
                    'value4': "Geração Reativa",
                    'value5': "Consumo Ativo",
                    'value6': "Consumo Reativo",
                    'value7': "Intervalos Faltantes",
                    'value8': "Status",
                    'value9': "Motivo"
                };

                return [objHeader, objValue, true];
            }
            else if (data.intervalSelected == "1hora" && data.typeMeter == "scde_origem") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: this.dateMask.transform(valueObj[i]['dataHora']),
                        value2: valueObj[i]['codigoInstalacao'] == "null" || valueObj[i]['codigoInstalacao'] == "" ? "-" : valueObj[i]['codigoInstalacao'],
                        value3: valueObj[i]['tipoEnergia'] == "null" || valueObj[i]['tipoEnergia'] == "" ? "-" : valueObj[i]['tipoEnergia'],
                        value4: this.valueMask.transform(valueObj[i]['geracaoAtiva'] == "null" ? "-" : valueObj[i]['geracaoAtiva']) + ' kWh',
                        value5: this.valueMask.transform(valueObj[i]['geracaoReativa'] == "null" ? "-" : valueObj[i]['geracaoReativa']) + ' kVArh',
                        value6: this.valueMask.transform(valueObj[i]['consumoAtivo'] == "null" ? "-" : valueObj[i]['consumoAtivo']) + ' kWh',
                        value7: this.valueMask.transform(valueObj[i]['consumoReativo'] == "null" ? "-" : valueObj[i]['consumoReativo']) + ' kVArh',
                        value8: valueObj[i]['origemColeta'] == "null" || valueObj[i]['origemColeta'] == "" ? "-" : valueObj[i]['origemColeta'],
                        value9: valueObj[i]['notificacaoColeta'] == "null" || valueObj[i]['notificacaoColeta'] == "" ? "-" : valueObj[i]['notificacaoColeta'],
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Código Instalação",
                    'value3': "Tipo Energia",
                    'value4': "Geração Ativa",
                    'value5': "Geração Reativa",
                    'value6': "Consumo Ativo",
                    'value7': "Consumo Reativo",
                    'value8': "Origem Coleta",
                    'value9': "Notificação Coleta"
                };

                return [objHeader, objValue, true];
            }
            else if (data.intervalSelected == "1dia" || data.intervalSelected == "1mes") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: this.dateMask.transform(valueObj[i]['dataHora']),
                        value2: this.valueMask.transform(valueObj[i]['geracaoAtiva'] == "null" ? "-" : valueObj[i]['geracaoAtiva']) + ' kWh',
                        value3: this.valueMask.transform(valueObj[i]['geracaoReativa'] == "null" ? "-" : valueObj[i]['geracaoReativa']) + ' kVArh',
                        value4: this.valueMask.transform(valueObj[i]['consumoAtivo'] == "null" ? "-" : valueObj[i]['consumoAtivo']) + ' kWh',
                        value5: this.valueMask.transform(valueObj[i]['consumoReativo'] == "null" ? "-" : valueObj[i]['consumoReativo']) + ' kVArh'
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Geração Ativa",
                    'value3': "Geração Reativa",
                    'value4': "Consumo Ativo",
                    'value5': "Consumo Reativo"
                };

                return [objHeader, objValue, true];
            }
        }
        else if (data.typeMeter == "hydro") {
            let valueObj = Object.values(data.dados);
            console.log('obj', valueObj);
            if (data.intervalSelected == "1hora") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: this.dateMask.transform(valueObj[i]['time']),
                        value2: data.temChuva ? valueObj[i]['rainfall'] == "null" ? "-" : valueObj[i]['rainfall'] + ' mm' : '-',
                        value3: data.temNivel ? valueObj[i]['stage'] == "null" ? "-" : valueObj[i]['stage'] + ' m³/s' : '-',
                        value4: data.temVazao ? valueObj[i]['flow'] == "null" ? "-" : valueObj[i]['flow'] + ' m' : '-',
                        value5: data.temVertimentos ? valueObj[i]['vertimento'] == "null" ? "-" : valueObj[i]['vertimento'] + ' m' : '-'
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Chuva",
                    'value3': "Nível",
                    'value4': "Vazão",
                    'value5': "Vertimento"
                };
                return [objHeader, objValue, true];
            }
            else if (data.intervalSelected == "1dia" || data.intervalSelected == "1mes") {
                for (var i = 0; i < valueObj.length; i++) {
                    objValue.push({
                        value1: this.dateMask.transform(valueObj[i]['time']),
                        value2: data.temChuva ? valueObj[i]['chuva'] == "null" ? "-" : valueObj[i]['chuva'] + ' mm' : '-',
                        value3: data.temNivel ? valueObj[i]['nivelMinimo'] == "null" ? "-" : valueObj[i]['nivelMinimo'] + ' m' : "-",
                        value4: data.temNivel ? valueObj[i]['nivelMedio'] == "null" ? "-" : valueObj[i]['nivelMedio'] + ' m' : "-",
                        value5: data.temNivel ? valueObj[i]['nivelMaximo'] == "null" ? "-" : valueObj[i]['nivelMaximo'] + ' m' : "-",
                        value6: data.temVazao ? valueObj[i]['vazaoMinimo'] == "null" ? "-" : valueObj[i]['vazaoMinimo'] + ' m³/s' : "-",
                        value7: data.temVazao ? valueObj[i]['vazaoMedio'] == "null" ? "-" : valueObj[i]['vazaoMedio'] + ' m³/s' : "-",
                        value8: data.temVazao ? valueObj[i]['vazaoMaximo'] == "null" ? "-" : valueObj[i]['vazaoMaximo'] + ' m³/s' : "-"
                    })
                };
                objHeader = {
                    'value1': "Data",
                    'value2': "Chuva",
                    'value3': "Nível Mínimo",
                    'value4': "Nível Médio",
                    'value5': "Nível Máximo",
                    'value6': "Vazão Mínimo",
                    'value7': "Vazão Médio",
                    'value8': "Vazão Máximo"
                };
                return [objHeader, objValue, true];
            }
        }
        // TODO: Descomentar após alteração na API do Leandro
        // else if (data.typeMeter == "variables_custom") {
        //     let valueObj = Object.values(data.dados);
        //     if (data.intervalSelected == "1hora") {
        //         for (var i = 0; i < valueObj.length; i++) {
        //             objValue.push({
        //                 value1: this.dateMask.transform(valueObj[i]['time']),
        //                 value2: data.temChuva ? valueObj[i]['rainfall'] == "null" ? "-" : valueObj[i]['rainfall'] + ' mm' : '-',
        //                 value3: data.temNivel ? valueObj[i]['stage'] == "null" ? "-" : valueObj[i]['stage'] + ' m³/s' : '-',
        //                 value4: data.temVazao ? valueObj[i]['flow'] == "null" ? "-" : valueObj[i]['flow'] + ' m' : '-',
        //                 value5: data.temVertimentos ? valueObj[i]['vertimento'] == "null" ? "-" : valueObj[i]['vertimento'] + ' m' : '-'
        //             })
        //         };
        // objHeader = {
        //     'value1': "a1",
        //     'value2': "a2",
        //     'value3': "a3",
        //     'value4': "a4",
        //     'value5': "a5",
        //     'value6': "a6",
        //     'value7': "a7",
        //     'value8': "a8",
        //     'value9': "d1",
        //     'value10': "d2",
        //     'value11': "d3",
        //     'value12': "d4"
        // };
        //         return [objHeader, objValue, true];
        //     }
        //     else if (data.intervalSelected == "1dia" || data.intervalSelected == "1mes") {
        //         for (var i = 0; i < valueObj.length; i++) {
        //             objValue.push({
        //                 value1: this.dateMask.transform(valueObj[i]['time']),
        //                 value2: data.temChuva ? valueObj[i]['chuva'] == "null" ? "-" : valueObj[i]['chuva'] + ' mm' : '-',
        //                 value3: data.temNivel ? valueObj[i]['nivelMinimo'] == "null" ? "-" : valueObj[i]['nivelMinimo'] + ' m' : "-",
        //                 value4: data.temNivel ? valueObj[i]['nivelMedio'] == "null" ? "-" : valueObj[i]['nivelMedio'] + ' m' : "-",
        //                 value5: data.temNivel ? valueObj[i]['nivelMaximo'] == "null" ? "-" : valueObj[i]['nivelMaximo'] + ' m' : "-",
        //                 value6: data.temVazao ? valueObj[i]['vazaoMinimo'] == "null" ? "-" : valueObj[i]['vazaoMinimo'] + ' m³/s' : "-",
        //                 value7: data.temVazao ? valueObj[i]['vazaoMedio'] == "null" ? "-" : valueObj[i]['vazaoMedio'] + ' m³/s' : "-",
        //                 value8: data.temVazao ? valueObj[i]['vazaoMaximo'] == "null" ? "-" : valueObj[i]['vazaoMaximo'] + ' m³/s' : "-"
        //             })
        //         };
        // objHeader = {
        //     'value1': "a1",
        //     'value2': "a2",
        //     'value3': "a3",
        //     'value4': "a4",
        //     'value5': "a5",
        //     'value6': "a6",
        //     'value7': "a7",
        //     'value8': "a8",
        //     'value9': "d1",
        //     'value10': "d2",
        //     'value11': "d3",
        //     'value12': "d4"
        // };
        //         return [objHeader, objValue, true];
        //     }
        // }
    }

    populateModalResume(typeResume, objResume) {
        let objModalTemp: DialogDataCollectModel = new DialogDataCollectModel;
        if (typeResume == "Energia") {
            let value1 = objResume.consumoPonta >= 1000 ? this.valueMask.transform((objResume.consumoPonta / 1000).toString()) + ' MWh' : this.valueMask.transform(objResume.consumoPonta) + " kWh";
            let value2 = objResume.consumoFora >= 1000 ? this.valueMask.transform((objResume.consumoFora / 1000).toString()) + ' MWh' : this.valueMask.transform(objResume.consumoFora) + " kWh";
            let value3 = objResume.geracaoPonta >= 1000 ? this.valueMask.transform((objResume.geracaoPonta / 1000).toString()) + ' MWh' : this.valueMask.transform(objResume.geracaoPonta) + " kWh";
            let value4 = objResume.geracaoFora >= 1000 ? this.valueMask.transform((objResume.geracaoFora / 1000).toString()) + ' MWh' : this.valueMask.transform(objResume.geracaoFora) + " kWh";

            objModalTemp.title = "Energia";
            objModalTemp.details.push({ title: 'Consumo Ponta', value: value1 })
            objModalTemp.details.push({ title: 'Consumo Fora', value: value2 })
            objModalTemp.details.push({ title: 'Geração Ponta', value: value3 })
            objModalTemp.details.push({ title: 'Geração Fora', value: value4 })

            return objModalTemp;
        }
        else if (typeResume == "Reativo") {
            let valueIndutivo = this.sumValuesString(objResume.kvarhIndutivoGeracao, objResume.kvarhCapacitivoConsumo);
            let valueCapacitivo = this.sumValuesString(objResume.kvarhIndutivoConsumo, objResume.kvarhCapacitivoGeracao);

            objModalTemp.title = "Reativo";
            objModalTemp.details.push({ title: 'DMCR', value: this.valueMask.transform(objResume.dmcr) + ' kW' })
            objModalTemp.details.push({ title: 'UFER', value: this.valueMask.transform(objResume.ufer) + ' kWh' })
            objModalTemp.details.push({ title: 'Reativo Indutivo', value: valueIndutivo > 1000 ? this.valueMask.transform((valueIndutivo / 1000).toString()) + " MVArh" : this.valueMask.transform(valueIndutivo.toString()) + " kVArh" })
            objModalTemp.details.push({ title: 'Reativo Capacitivo', value: valueCapacitivo > 1000 ? this.valueMask.transform((valueCapacitivo / 1000).toString()) + " MVArh" : this.valueMask.transform(valueCapacitivo.toString()) + " kVArh" })

            return objModalTemp;
        }
        else if (typeResume == "Demanda") {
            let demandaPonta = this.obtainGreatestValue(objResume.demandaMaximaConsumoPonta, objResume.demandaMaximaGeracaoPonta);
            let demandaFora = this.obtainGreatestValue(objResume.demandaMaximaConsumoFora, objResume.demandaMaximaGeracaoFora);
            let timeDemandaPonta = this.obtainGreatestValue(objResume.timeDemandaMaximaConsumoPonta, objResume.timeDemandaMaximaGeracaoPonta);
            let timeDemandaFora = this.obtainGreatestValue(objResume.timeDemandaMaximaConsumoFora, objResume.timeDemandaMaximaGeracaoFora);
            let demandaContratadaPonta = this.obtainGreatestValue(objResume.demandaContratadaConsumoPonta, objResume.demandaContratadaGeracaoPonta);
            let demandaContratadaFora = this.obtainGreatestValue(objResume.demandaContratadaConsumoFora, objResume.demandaContratadaGeracaoFora);
            let fatorCargaPonta = this.obtainGreatestValue(objResume.fatorCargaConsumoPonta, objResume.fatorCargaGeracaoPonta);
            let fatorCargaFora = this.obtainGreatestValue(objResume.fatorCargaConsumoFora, objResume.fatorCargaGeracaoFora);

            objModalTemp.title = "Energia";
            objModalTemp.details.push({ title: 'Demanda Máx. Ponta', value: this.valueMask.transform(demandaPonta) + " kW" })
            objModalTemp.details.push({ title: 'Hora Demanda Máx. Ponta', value: this.dateMask.transform(timeDemandaPonta) })
            objModalTemp.details.push({ title: 'Demanda Máx. Fora', value: this.valueMask.transform(demandaFora) + " kW" })
            objModalTemp.details.push({ title: 'Hora Demanda Máx. Fora', value: this.dateMask.transform(timeDemandaFora) })
            objModalTemp.details.push({ title: 'Contratada Ponta', value: this.valueMask.transform(demandaContratadaPonta) + " kW" })
            objModalTemp.details.push({ title: 'Contratada Fora', value: this.valueMask.transform(demandaContratadaFora) + " kW" })
            objModalTemp.details.push({ title: 'Fator Carga Ponta', value: this.valueMask.transform(fatorCargaPonta) + " %" })
            objModalTemp.details.push({ title: 'Fator Carga Fora', value: this.valueMask.transform(fatorCargaFora) + " %" })

            return objModalTemp;
        }
        else if (typeResume == "Energia SCDE") {
            // operador ternário sim
            let consumoScdeAtivo = objResume.scdeData.consumoAtivo != null ? Number(objResume.scdeData.consumoAtivo) > 1000 ? this.valueMask.transform((objResume.scdeData.consumoAtivo / 1000).toString()) + ' MWh' : this.valueMask.transform((objResume.scdeData.consumoAtivo).toString()) + ' kWh' : "-";
            let consumoScdeReativo = objResume.scdeData.consumoReativo != null ? Number(objResume.scdeData.consumoReativo) > 1000 ? this.valueMask.transform((objResume.scdeData.consumoReativo / 1000).toString()) + ' MWh' : this.valueMask.transform((objResume.scdeData.consumoReativo).toString()) + ' kWh' : "-";
            let consumoScdeAtivoAjustado = objResume.scdeData.consumoAtivoAjustado != null ? Number(objResume.scdeData.consumoAtivoAjustado) > 1000 ? this.valueMask.transform((objResume.scdeData.consumoAtivoAjustado / 1000).toString()) + ' MWh' : this.valueMask.transform((objResume.scdeData.consumoAtivoAjustado).toString()) + ' kWh' : "-";
            let consumoScdeReativoAjustado = objResume.scdeData.consumoReativoAjustado != null ? Number(objResume.scdeData.consumoReativoAjustado) > 1000 ? this.valueMask.transform((objResume.scdeData.consumoReativoAjustado / 1000).toString()) + ' MWh' : this.valueMask.transform((objResume.scdeData.consumoReativoAjustado).toString()) + ' kWh' : "-";
            let geracaoScdeAtiva = objResume.scdeData.geracaoAtiva != null ? Number(objResume.scdeData.geracaoAtiva) > 1000 ? this.valueMask.transform((objResume.scdeData.geracaoAtiva / 1000).toString()) + ' MWh' : this.valueMask.transform((objResume.scdeData.geracaoAtiva).toString()) + ' kWh' : "-";
            let geracaoScdeReativa = objResume.scdeData.geracaoReativa != null ? Number(objResume.scdeData.geracaoReativa) > 1000 ? this.valueMask.transform((objResume.scdeData.geracaoReativa / 1000).toString()) + ' MWh' : this.valueMask.transform((objResume.scdeData.geracaoReativa).toString()) + ' kWh' : "-";
            let geracaoScdeAtivaAjustada = objResume.scdeData.geracaoAtivaAjustada != null ? Number(objResume.scdeData.geracaoAtivaAjustada) > 1000 ? this.valueMask.transform((objResume.scdeData.geracaoAtivaAjustada / 1000).toString()) + ' MWh' : this.valueMask.transform((objResume.scdeData.geracaoAtivaAjustada).toString()) + ' kWh' : "-";
            let geracaoScdeReativaAjustada = objResume.scdeData.geracaoReativaAjustada != null ? Number(objResume.scdeData.geracaoReativaAjustada) > 1000 ? this.valueMask.transform((objResume.scdeData.geracaoReativaAjustada / 1000).toString()) + ' MWh' : this.valueMask.transform((objResume.scdeData.geracaoReativaAjustada).toString()) + ' kWh' : "-";
            let horasAjustadas = objResume.scdeData.horasAjustadas != "" || objResume.scdeData.horasAjustadas != null ? objResume.scdeData.horasAjustadas : "-";

            objModalTemp.title = "Energia SCDE";
            objModalTemp.details.push({ title: 'Consumo Ponta', value: objResume.consumoPonta })
            objModalTemp.details.push({ title: 'Consumo Fora', value: objResume.consumoFora })
            objModalTemp.details.push({ title: 'Geração Ponta', value: objResume.geracaoPonta })
            objModalTemp.details.push({ title: 'Geração Fora', value: objResume.geracaoFora })
            objModalTemp.details.push({ title: 'Consumo SCDE Ativo', value: consumoScdeAtivo })
            objModalTemp.details.push({ title: 'Consumo SCDE Reativo', value: consumoScdeReativo })
            objModalTemp.details.push({ title: 'Consumo SCDE Ativo Ajustado', value: consumoScdeAtivoAjustado })
            objModalTemp.details.push({ title: 'Consumo SCDE Reativo Ajustado', value: consumoScdeReativoAjustado })
            objModalTemp.details.push({ title: 'Geração SCDE Ativa', value: geracaoScdeAtiva })
            objModalTemp.details.push({ title: 'Geração SCDE Reativa', value: geracaoScdeReativa })
            objModalTemp.details.push({ title: 'Geração SCDE Ativa Ajustada', value: geracaoScdeAtivaAjustada })
            objModalTemp.details.push({ title: 'Geração SCDE Reativa Ajustada', value: geracaoScdeReativaAjustada })
            objModalTemp.details.push({ title: 'Horas Ajustadas', value: horasAjustadas })

            return objModalTemp;
        }
        else if (typeResume == "Chuva") {
            let value1 = this.valueMask.transform(objResume.rainfall);

            objModalTemp.title = "Chuva";
            objModalTemp.details.push({ title: 'Total', value: value1 + ' mm' })

            return objModalTemp;
        }
        else if (typeResume == "Nivel") {
            let dateMin = objResume.dataStageMin == "" || objResume.dataStageMin == null || objResume.dataStageMin == "null" ? "--/--/---- --:--" : objResume.dataStageMin
            let dateMax = objResume.dataStageMin == "" || objResume.dataStageMin == null || objResume.dataStageMin == "null" ? "--/--/---- --:--" : objResume.dataStageMin

            objModalTemp.title = "Nível";
            objModalTemp.details.push({ title: 'Momento Nível Mín.', value: dateMin })
            objModalTemp.details.push({ title: 'Momento Nível Máx.', value: dateMax })

            return objModalTemp;
        }
        else if (typeResume == "Vazao") {
            let dateMin = objResume.dataFlowMin == "" || objResume.dataFlowMin == null || objResume.dataFlowMin == "null" ? "--/--/---- --:--" : objResume.dataFlowMin
            let dateMax = objResume.dataFlowMax == "" || objResume.dataFlowMax == null || objResume.dataFlowMax == "null" ? "--/--/---- --:--" : objResume.dataFlowMax

            objModalTemp.title = "Nível";
            objModalTemp.details.push({ title: 'Momento Nível Mín.', value: dateMin })
            objModalTemp.details.push({ title: 'Momento Nível Máx.', value: dateMax })

            return objModalTemp;
        }
    }

    populateGuarantee(objResume) {
        return [this.valueMask.transform(objResume.garantiaFisica) + ' MW', this.valueMask.transform((objResume.mediaGerada / 1000).toString()) + ' MW'];
    }

    /**
   * The method find a highest value between 'fatorCargaConsumoPonta' and 'fatorCargaGeracaoPonta' and 'fatorCargaConsumoFora' and 'fatorCargaGeracaoFora'.
   * @author Wagner Castilho
   *
   * @params object - object of API
   */
    obterFatorCargaPontaFora(object) {
        if (Number(object.fatorCargaConsumoPonta) >= Number(object.fatorCargaGeracaoPonta)) {
            object.fatorCargaPonta = object.fatorCargaConsumoPonta;
        }
        else {
            object.fatorCargaPonta = object.fatorCargaGeracaoPonta;
        }

        if (Number(object.fatorCargaConsumoFora) >= Number(object.fatorCargaGeracaoFora)) {
            object.fatorCargaFora = object.fatorCargaConsumoFora;
        }
        else {
            object.fatorCargaFora = object.fatorCargaGeracaoFora;
        }

        return object.fatorCargaPonta >= object.fatorCargaFora ? object.fatorCargaPonta : object.fatorCargaFora;

        // // Tratamento para verificar o maior do fator carga ponta/fora (usado no gráfico de fator de carga)
        // if (object.fatorCargaPonta > object.fatorCargaFora) {
        //     object.fatorCarga = object.fatorCargaPonta;
        // } else if (object.fatorCargaFora > object.fatorCargaPonta) {
        //     object.fatorCarga = object.fatorCargaFora;
        // } else {
        //     object.fatorCarga = object.fatorCargaFora;
        // }
    }

    /**
   * The method find a highest value between 'demandaContratadaConsumoPonta' and 'demandaContratadaGeracaoPonta' and 'demandaContratadaConsumoFora' and 'demandaContratadaGeracaoFora'.
   * @author Wagner Castilho
   *
   * @params object - object of API
   */
    obtainGreatestValue(valueA, valueB) {
        if (valueA == "null" && valueB == "null") {
            return "-";
        } else if (valueA == "null" && valueB != "null") {
            return valueB;
        } else if (valueA != "null" && valueB == "null") {
            return valueA;
        }
        else {
            return Number(valueA) >= Number(valueB) ? valueA : valueB;
        }
    }

    /**
  * The method sum two values which are string and return a int value
  * @author Wagner Camargo Castilho
  *
  */
    sumValuesString(value1, value2) {
        return parseInt(value1) + parseInt(value2);
    }

}

interface PeriodicElement {
    value1?: string;
    value2?: string;
    value3?: string;
    value4?: string;
    value5?: string;
    value6?: string;
    value7?: string;
    value8?: string;
    value9?: string;
    value10?: string;
    value11?: string;
    value12?: string;
    value13?: string;
    value14?: string;
    value15?: string;
    value16?: string;
    value17?: string;
    value18?: string;
    value19?: string;
    value20?: string;
    demandOutValue?: string;
    timeDemandOutValue?: string;
    demandInValue?: string;
    timeDemandInValue?: string;
}