

export class ApiParamsModel {
    siteId: string;
    meterId: string;
    hydroId: string;
    dateInitial: string;
    dateFinally: string;
    endpoint: string;
    type: string;
    intervalSelected: string;
}