

export interface MetersModel {
    id: string;
    hydroId?: string;
    value: string;
    selected: boolean;
    rainfall?: string;
    stage?: string;
    flow?: string;
}