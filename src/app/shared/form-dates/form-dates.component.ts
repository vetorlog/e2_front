import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { ApiService } from 'src/app/services/api.service';
import { EndPoint } from '../constants/end-point';
import { Util } from '../utils/util';
import { IntervalModel } from './model-interval';
import { MetersModel } from './model-meters';
import { FormDatesService } from 'src/app/services/form-dates.service';
import { ErrorStateMatcher, ThemePalette } from '@angular/material/core';
import { ApiParamsModel } from './params-api.model';
import { ActivatedRoute } from '@angular/router';
import { ExcelExportService } from 'src/app/services/excel-export.service';
import { ToastrService } from 'ngx-toastr';

/** O controle de erro se torna inválido quando ele for selecionado e não preenchido ou o form for submitido */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'emeter2-form-dates',
  templateUrl: './form-dates.component.html',
  styleUrls: ['./form-dates.component.scss']
})
export class FormDatesComponent implements OnInit {

  @Input() siteId: string;
  @Input() hydroId: string;
  @Output() resumOutput: EventEmitter<any> = new EventEmitter();
  @Output() fullDataOutput: EventEmitter<any> = new EventEmitter();

  constructor(
    private apiService: ApiService,
    private util: Util,
    private formDatesService: FormDatesService,
    private activatedRoute: ActivatedRoute,
    private excelExport: ExcelExportService,
    private toast: ToastrService) { }

  color: ThemePalette = 'accent';
  dateInitial: Date;
  dateFinally: Date;
  formCollect: FormGroup;
  intervalsCheckboxArray: IntervalModel[] = [
    { id: "5min", value: "5 min.", disabled: true },
    { id: "15min", value: "15 min.", disabled: false },
    { id: "1hora", value: "1 hora", disabled: false },
    { id: "1dia", value: "1 dia", disabled: false },
    { id: "1mes", value: "1 mês", disabled: false }
  ];
  metersCombo: MetersModel[] = [];
  matcher = new MyErrorStateMatcher();
  paramsResum: ApiParamsModel = new ApiParamsModel();
  typeRequestToSend: string;
  buttonLoading: boolean = false;
  fullDataToExport: any;
  resumDataToExport: any;
  activeBtnExcel: boolean = false;
  showScdeMeter: boolean = false;

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => this.siteId = params.id); // obtendo o siteId passado por parâmetro na rota da URL
    this.setFormGroup(); // inicializa as variáveis usadas no HTML
    this.getAndSetParams(); // busca e seta os dados necessários para o form
  }

  async getAndSetParams() {
    await this.checkPermissionPoint("scde");
    this.getAndSetMeters(); // busca e seta os valores da combo de medição -- getHydroAndVarCustom é chamado aqui
  }

  setFormGroup() {
    const { dateTimeInit, dateTimeEnd } = this.util.getInitEndDateTime();
    this.formCollect = new FormGroup({
      dateInitial: new FormControl(new Date(dateTimeInit), [Validators.required]), //TODO: verificar se não vai dar problema em dias como 31 ou 30
      dateFinally: new FormControl(new Date(dateTimeEnd), [Validators.required]), //TODO: verificar se não vai dar problema em dias como 31 ou 30
      medidorSelect: new FormControl("medidas_consistidas", [Validators.required]),
      intervaloCheckbox: new FormControl("1hora", [Validators.required])
    });
  }

  exportExcel() {
    let nameFileToExport = "Coleta-de-Dados " + new Date().toLocaleDateString("pt-BR") + ".xlsx";
    this.excelExport.exportTableDataCollect(Object.values(this.fullDataToExport.dataMetersIntegralizadas), this.resumDataToExport, nameFileToExport);
  }

  async sendValues(objForm: FormGroup) {
    this.activeBtnExcel = false;
    let resumDataScde;
    this.activeLoaderBtn(true);
    // #region Início DE-PARA objeto de parâmetro para API
    let medidorSelect = objForm.value.medidorSelect;
    this.paramsResum.siteId = this.siteId;
    this.paramsResum.meterId = objForm.value.medidorSelect;
    this.paramsResum.dateInitial = this.util.convertDateToSendApi(objForm.value.dateInitial);
    this.paramsResum.dateFinally = this.util.convertDateToSendApi(objForm.value.dateFinally);
    this.paramsResum.intervalSelected = objForm.value.intervaloCheckbox;
    this.paramsResum.type = medidorSelect.search("_hydro") != -1
      ? "hydro" : medidorSelect.search("_customVar") != -1 ? "variablesCustom" : "energy";
    this.paramsResum.hydroId = medidorSelect.search("_hydro") != -1 || medidorSelect.search("_customVar") != -1 ? medidorSelect.split("_")[0] : "";

    // tratamento para o getFullData
    let typeMeter = medidorSelect.search("_hydro") != -1 ? "hydro" : medidorSelect.search("_customVar") != -1 ? "variables_custom"
      : medidorSelect.includes("scde_consolidado") ? "scde_consolidado" : medidorSelect.includes("scde_origem") ? "scde_origem" :
        isNaN(medidorSelect) ? "consistidas" : "principal_retaguarda";
    //#endregion Fim DE-PARA objeto de parâmetro para API

    //#region Início chamada de requests
    if (typeMeter.includes("scde")) {
      resumDataScde = await this.getResumScde(this.paramsResum).toPromise().then(
        dataScde => {
          return dataScde;
        },
        err => {
          return null;
        }
      )
    }

    this.getResum(this.paramsResum).subscribe(
      resumData => {
        if (resumData == "null" || resumData == null || resumData == undefined) {
          resumData = null;
        } else {
          resumData['typeMeter'] = typeMeter.includes("hydro") || typeMeter.includes("variables_custom") ? "hydro"
            : typeMeter.includes("scde") ? "scde" : "energy";
          resumData['scdeData'] = resumData['typeMeter'] == "scde" ? resumDataScde : null;
          this.resumOutput.emit(resumData);
          this.resumDataToExport = resumData;
        }
      },
      err => {
        this.activeLoaderBtn(false);
        this.resumOutput.emit(null);
      }
    )

    this.getFullData(this.paramsResum, typeMeter).subscribe(
      fullData => {
        this.activeLoaderBtn(false);
        if (fullData == "null" || fullData == null || fullData == undefined) {
          fullData = null;
        } else {
          fullData['typeMeter'] = typeMeter;
          fullData['intervalSelected'] = objForm.value.intervaloCheckbox;
        }
        this.fullDataOutput.emit(fullData);
        this.fullDataToExport = fullData;
        this.activeBtnExcel = true;
      },
      err => {
        this.fullDataOutput.emit(null);
        this.activeLoaderBtn(false);
      }
    )
    //#endregion Fim chamada de requests
  }

  //#region SET AND GET FUNCTIONS
  getHydroAndVarCustom() {
    let params = new HttpParams()
      .set('site_id', this.siteId.toString());

    this.apiService.get(EndPoint.DADOS_ESTACAO, params).subscribe(
      dadosHydro => {
        Object.values(dadosHydro['hydros']).forEach(hydro => {
          let objName = hydro['name'].toLowerCase(); // tratamento da primeira letra ficar maiúscula e o restante minúscula.
          if (hydro['elipse'] != null) {
            // TODO: Descomentar o código abaixo após Leandro inserir o header do customvariables em um request apenas
            // if (this.util.checkVariablesCustom(hydro['elipse'])) {
            //   this.metersCombo.push(
            //     {
            //       id: hydro['id'] + "_customVar",
            //       value: objName.charAt(0).toUpperCase() + objName.slice(1) + " (Var. Customizável)",
            //       hydroId: hydro['elipse']['siteHydroId'],
            //       selected: false
            //     });
            // }
          }
          this.metersCombo.push(
            {
              id: hydro['id'] + "_hydro",
              value: objName.charAt(0).toUpperCase() + objName.slice(1) + " (Hidrologia)",
              rainfall: hydro['rainfall'],
              stage: hydro['stage'],
              flow: hydro['flow'],
              selected: false
            }
          );
        });
      }
    )
  }

  async getAndSetMeters() {
    const meters = <any>await this.formDatesService.callMeters(this.siteId);
    const keysMeters = Object.keys(meters);
    for (let i = 0; i < keysMeters.length; i++) {
      if (meters[keysMeters[i]].meterRoleName.toLowerCase() == "main") {
        this.metersCombo.push({ id: meters[keysMeters[i]].id, value: 'Medidor Principal (energia)', selected: false });
      } else if (meters[keysMeters[i]].meterRoleName.toLowerCase() == "backup") {
        this.metersCombo.push({ id: meters[keysMeters[i]].id, value: 'Medidor Retaguarda (energia)', selected: false });
      } else {
        this.metersCombo.push({ id: meters[keysMeters[i]].id, value: meters[keysMeters[i]].meterRoleName + ' (energia)', selected: false });
      }
    }
    //Verifica se existe o produto SCDE para adicionar a combo do medidor
    if (this.showScdeMeter) {
      this.metersCombo.push(
        { id: "medidas_consistidas", value: "Medidas Consistidas (energia)", selected: true },
        { id: "scde_consolidado", value: "SCDE Medidas Consolidadas (energia)", selected: false },
        { id: "scde_origem", value: "SCDE Origem de Dados (energia)", selected: false }
      );
    } else {
      this.metersCombo.push(
        { id: "medidas_consistidas", value: "Medidas Consistidas (energia)", selected: true }
      );
    }
    this.getHydroAndVarCustom(); // obtem os medidores de hydro e se tiver, de variáveis customizáveis
  }

  getResum(objectResum: ApiParamsModel): Observable<any> {
    let typeResum = this.paramsResum.type;
    var params = new HttpParams()
      .set('site_id', objectResum.siteId);

    objectResum.endpoint = typeResum == "energy" ? EndPoint.RESUMO_ENERGIA :
      typeResum == "hydro" ? EndPoint.RESUMO_HIDRO : EndPoint.HIDRO_CUSTOMIZAVEIS_RESUMO;

    if (typeResum == "energy") {
      params = params.append("inicio", objectResum.dateInitial)
      params = params.append("fim", objectResum.dateFinally)
      params = params.append("fonte_dados", "hourly");
    } else if (typeResum == "hydro" || typeResum == "variablesCustom") {
      params = params.append("hydro_id", objectResum.hydroId)
      params = params.append("data_inicial", objectResum.dateInitial + " 00:00:00")
      params = params.append("data_final", objectResum.dateFinally + " 00:00:00");
    }
    return this.apiService.get(objectResum.endpoint, params);
  }

  getResumScde(objectResum: ApiParamsModel) {
    let params = new HttpParams()
      .set("site_id", objectResum.siteId)
      .set("inicio", objectResum.dateInitial)
      .set("fim", objectResum.dateFinally)
      .set("tipo", objectResum.type)
      .set("group_by", 'horario');

    return this.apiService.get(EndPoint.DADOS_SCDE_ACUMULADO, params)
  }

  getFullData(objToSendApi: ApiParamsModel, typeRequest): Observable<any> {
    let { interval, dataSource } = this.util.getIntervalAndDataSource(objToSendApi.intervalSelected);
    var params = new HttpParams()
      .set('site_id', objToSendApi.siteId);
    if (typeRequest == "consistidas") {
      params = params.append("inicio", objToSendApi.dateInitial)
      params = params.append("fim", objToSendApi.dateFinally)
      params = params.append("fonte_dados", dataSource)
      params = params.append("intervalo", interval);
    } else if (typeRequest == "hydro" || typeRequest == "variables_custom") {
      let frequency = objToSendApi.intervalSelected == "1hora"
        ? "HOURLY" : objToSendApi.intervalSelected == "1dia" ? "DAILY" : "MONTHLY";
      params = params.append("hydro_id", objToSendApi.hydroId)
      params = params.append("data_inicial", objToSendApi.dateInitial + " 00:00:00")
      params = params.append("data_final", objToSendApi.dateFinally + " 00:00:00")
      params = params.append("frequency", frequency);
    } else if (typeRequest == "scde_consolidado" || typeRequest == "scde_origem") {
      let typeScde = typeRequest.toLowerCase().includes("origem") ? "origem" : "scde";
      params = params.append("inicio", objToSendApi.dateInitial)
      params = params.append("fim", objToSendApi.dateFinally)
      params = params.append("tipo", typeScde)
      params = params.append("group_by", 'horario');
    } else {
      params = params.delete("site_id"); //remover o site id
      params = params.append("site_meter_id", objToSendApi.meterId) //id combo meter
      params = params.append("inicio", objToSendApi.dateInitial)
      params = params.append("fim", objToSendApi.dateFinally)
      params = params.append("fonte_dados", "data_meters")
      params = params.append("intervalo", interval);
    }

    objToSendApi.endpoint = typeRequest == "hydro" ? EndPoint.DADOS_HIDRO :
      typeRequest == "variables_custom" ? EndPoint.HIDRO_CUSTOMIZAVEIS_DADOS :
        typeRequest.includes("scde") ? EndPoint.DADOS_SCDE : EndPoint.DADOS_MEDICAO;

    return this.apiService.get(objToSendApi.endpoint, params);
  }

  checkPermissionPoint(typePermission: string) {
    let params = new HttpParams()
      .set("site_id", this.siteId.toString())
      .set("produto_nome", typePermission);

    this.apiService.get(EndPoint.VERIFICAR_PERMISSAO_PONTO, params).subscribe(
      data => {
        if (Object.values(data['produtos']).length > 0) {
          this.showScdeMeter = true;
        } else {
          this.toast.info("Não foi encontrado o produto SCDE para este ponto", "SCDE");
          this.showScdeMeter = false;
        }
      },
      err => {
        this.toast.error("Erro ao buscar permissionamento de produto", "Produto");
        console.log('err', err);
      }
    )
  }

  /**
   * The method filters the interval according to the meter selected.
   * @author Wagner Castilho
   * @params value - is the value that was selected in the meter combo (html)
   */
  setIntervals(meterSelected) {
    // Verificação com IsNaN porque quando é principal/retaguarda/ou outro medidor que venha da API, é salvo como number o ID
    // sendo assim, só irá entrar no primeiro IF caso tiver número na string informada.
    if (!isNaN(meterSelected)) {
      this.intervalsCheckboxArray.forEach(element => {
        element.disabled = false;
      });
    }
    else if (meterSelected == "medidas_consistidas") {
      this.formCollect.patchValue({ intervaloCheckbox: '1hora' }); // atualizar o valor do checkbox
      this.intervalsCheckboxArray.find(f => f.id == "5min").disabled = true;
      this.intervalsCheckboxArray.find(f => f.id == "15min").disabled = false;
    }
    else {
      this.formCollect.patchValue({ intervaloCheckbox: '1hora' }); // atualizar o valor do checkbox
      this.intervalsCheckboxArray.find(f => f.id == "5min").disabled = true;
      this.intervalsCheckboxArray.find(f => f.id == "15min").disabled = true;
    }
  }
  //#endregion

  //#region Active loader button
  activeLoaderBtn(active) {
    if (active) {
      this.buttonLoading = true;
      document.getElementById("searchButton").classList.add("active");
    }
    else {
      document.getElementById("searchButton").classList.add("success");
      setTimeout(() => {
        document.getElementById("searchButton").classList.remove("success");
        document.getElementById("searchButton").classList.remove("active");
      }, 3000);
      this.buttonLoading = false;
    }

  }
  //#endregion Active loader button
}
