

export interface IntervalModel {
    id: string,
    value: string,
    disabled: boolean
}