import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'emeter2-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardDetailsComponent implements OnInit {

  @Input() titleMain: string;
  @Input() titleSecondary: string;
  @Input() typeInfo: string;
  @Input() segmentInfo: string;
  @Input() city: string;
  @Input() typeIcon: string;

  constructor() { }

  ngOnInit(): void {
  }

}
