import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'MaskValue' })
export class ValueMaskPipe implements PipeTransform {
    transform(value: string): string {
        if (value === null || value === "null") {
            return " - ";
        } else if (value) {
            let parseValue = parseFloat(value);

            if (isNaN(parseValue)) {
                return " - "
            } else {
                return parseValue.toLocaleString('pt-BR', { minimumFractionDigits: 3 })
            }
        } else {
            return value
        }
    }
}

@Pipe({
    name: 'DateWithoutSecondMaskPipe'
})
export class DateWithoutSecondMaskPipe implements PipeTransform {
    transform(value: string): string {
        if (value === null || value === "null") {
            return "--/--/---- --:--";
        } else if (value) {
            var teste = value.substring(0, 16);
            return teste;
        } else {
            return value
        }
    }
}

@Pipe({
  name: 'searchGruposPontos'
})
export class SearchPipe implements PipeTransform {
  public transform(value, term: string) {
    if (!term) return value;
    return value.filter(item => item.groupsite.name.toLowerCase().includes(term.toLowerCase()))
    // return (value || []).filter(item => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));

  }
}