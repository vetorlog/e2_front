import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableDataCollectComponent } from './table-data-collect.component';

describe('TableDataCollectComponent', () => {
  let component: TableDataCollectComponent;
  let fixture: ComponentFixture<TableDataCollectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableDataCollectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableDataCollectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
