import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardResumeInfoComponent } from './card-resume-info/card-resume-info.component';
import { CardDetailsComponent } from './card-details/card-details.component';
import { CardEnergyComponent } from './card-energy/card-energy.component';
import { CardGuaranteeComponent } from './card-guarantee/card-guarantee.component';
import { FormDatesComponent } from './form-dates/form-dates.component';
import { AngularMaterialModule } from 'src/assets/angular-material-module/angular-material.module';
import { TableDataCollectComponent } from './table-data-collect/table-data-collect.component';
import { MapsComponent } from './maps/maps.component';
import { GoogleMapsModule, GoogleMap } from '@angular/google-maps'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValueMaskPipe, DateWithoutSecondMaskPipe, SearchPipe } from './pipe/pipe';
import { GenerationManagementComponent } from './management-landing/generation-management/generation-management.component';
import { HydroManagementComponent } from './management-landing/hydro-management/hydro-management.component';
import { EconometerManagementComponent } from './management-landing/econometer-management/econometer-management.component';
import { DistributionManagementComponent } from './management-landing/distribution-management/distribution-management.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { DialogDataCollectComponent } from './dialog-modal/dialog-data-collect/dialog-data-collect.component';

@NgModule({
  declarations: [
    CardResumeInfoComponent,
    CardDetailsComponent,
    CardEnergyComponent,
    CardGuaranteeComponent,
    FormDatesComponent,
    TableDataCollectComponent,
    MapsComponent,
    ValueMaskPipe,
    DateWithoutSecondMaskPipe,
    GenerationManagementComponent,
    HydroManagementComponent,
    SearchPipe,
    EconometerManagementComponent,
    DistributionManagementComponent,
    ButtonsComponent,
    DialogDataCollectComponent,
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    GoogleMapsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CardResumeInfoComponent,
    CardDetailsComponent,
    CardEnergyComponent,
    CardGuaranteeComponent,
    FormDatesComponent,
    TableDataCollectComponent,
    MapsComponent,
    GoogleMap,
    ValueMaskPipe,
    DateWithoutSecondMaskPipe,
    GenerationManagementComponent,
    HydroManagementComponent,
    SearchPipe,
    EconometerManagementComponent,
    DistributionManagementComponent,
  ],
  providers: [
    ValueMaskPipe
  ],
  entryComponents: [
    DialogDataCollectComponent
  ]
})
export class SharedModule { }
