import { Component, OnInit } from '@angular/core';
import { EndPoint } from '../constants/end-point';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'emeter2-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {

  constructor(private apiService: ApiService) { }

  zoom = 4;
  center: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    mapTypeId: 'hybrid',
    maxZoom: 15,
    minZoom: 8,
  };
  markers = [];

  ngOnInit(): void {
    navigator.geolocation.getCurrentPosition(position => {
      this.center = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      }
    })

    this.getMarkersMap();
  }

  getMarkersMap() {
    this.apiService.get(EndPoint.PONTOS_MAPA).subscribe(
      data => {
        console.log("data", data);
        for (var i = 0; i < data['listConsumidor'].length; i++) {
          this.markers.push({
            position: {
              lat: parseFloat(data['listConsumidor'][i].latitude),
              lng: parseFloat(data['listConsumidor'][i].longitude)
            },
            // label: {
            //   color: 'red',
            //   text: data['listConsumidor'][i].distribuidora,
            // },
            // title: data['listConsumidor'][i].nome,
            // options: { animation: google.maps.Animation.BOUNCE },
          })
        }

      }
    );
  }

}
