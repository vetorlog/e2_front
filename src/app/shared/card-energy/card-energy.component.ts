import { Component, OnInit, Input } from '@angular/core';
import { DialogDataCollectModel } from '../model/dialog-data-collect';
import { DialogDataCollectComponent } from '../dialog-modal/dialog-data-collect/dialog-data-collect.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'emeter2-card-energy',
  templateUrl: './card-energy.component.html',
  styleUrls: ['./card-energy.component.scss']
})
export class CardEnergyComponent implements OnInit {
  @Input() objResum;
  @Input() objModal;

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  callModal() {
    this.dialog.open(DialogDataCollectComponent, { data: this.objModal })
  }

}
