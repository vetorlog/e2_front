import { environment } from 'src/environments/environment.prod';
// import { environment } from 'src/environments/environment.dev';
// import { environment } from 'src/environments/environment'

export class EndPoint {

    // AUTH
    public static API_AUTHENTICATION = environment.apiUrl + 'v1/autenticacao/token';

    // USER
    public static DADOS_USUARIO = environment.apiUrl + 'v1/usuario';

    // SENHA
    public static MUDAR_SENHA = environment.apiUrl + 'v1/autenticacao/trocasenha'

    // GROUP
    public static GRUPOS_PONTOS = environment.apiUrl + 'v1/ponto/grupospontos'

    // SITE
    public static DADOS_SITE = environment.apiUrl + 'v1/ponto';
    public static SITES_LIST_MENU = environment.apiUrl + 'v1/ponto/simples';

    // ESTAÇÃO
    public static DADOS_ESTACAO = environment.apiUrl + 'v1/ponto/estacao';

    // ENERGY
    public static GAUGE_GERACAO = environment.apiUrl + 'v1/medicoes/gaugeGeracao';
    public static DADOS_MEDICAO = environment.apiUrl + 'v1/medicoes/dadosMedicoes';
    public static DADOS_SCDE = environment.apiUrl + 'v1/medicoes/dadosScde';
    public static RESUMO_ENERGIA = environment.apiUrl + 'v1/medicoes/dadosAcumuladosMedicoes';
    public static DADOS_SCDE_ACUMULADO = environment.apiUrl + 'v1/medicoes/dadosScdeAcumulados';

    // HIDRO
    public static RESUMO_HIDRO = environment.apiUrl + 'v1/hidrologia/resumo';
    public static DADOS_HIDRO = environment.apiUrl + 'v1/hidrologia/dados';
    public static HIDRO_CUSTOMIZAVEIS_RESUMO = environment.apiUrl + 'v1/hidrologia/customizaveis/resumo';
    public static HIDRO_CUSTOMIZAVEIS_DADOS = environment.apiUrl + 'v1/hidrologia/customizaveis/dados';

    // PREVISÃO TEMPO
    public static PREVISAO_TEMPO = environment.apiUrl + 'v1/hidrologia/previsaoTempo';

    // Medidores
    public static MEDIDORES = environment.apiUrl + 'v1/ponto/medidores';
    public static DADOS_COMPARATIVOS = environment.apiUrl + 'v1/medicoes/dadosMedicoesComparativos';
    // Mapa
    public static CONSUMIDOR_MAPA = environment.apiUrl + 'v1/ponto/mapa?pontos_tipo=consumidor';
    public static GERADORES_HIDROELETRICO_MAPA = environment.apiUrl + 'v1/ponto/mapa?pontos_tipo=gerador-hidreletrico';
    public static GERADORES_TERMICO_MAPA = environment.apiUrl + 'v1/ponto/mapa?pontos_tipo=gerador-termico';
    public static GERADORES_SOLAR_MAPA = environment.apiUrl + 'v1/ponto/mapa?pontos_tipo=gerador-solar';
    public static SUBESTACAO_MAPA = environment.apiUrl + 'v1/ponto/mapa?pontos_tipo=subestacao';
    public static ESTACAO_MAPA = environment.apiUrl + 'v1/ponto/mapa?pontos_tipo=estacao';
    public static EOLICA_MAPA = environment.apiUrl + 'v1/ponto/mapa?pontos_tipo=eolica';
    public static PONTOS_MAPA = environment.apiUrl + 'v1/ponto/mapanovo';

    // Digest
    public static DIGEST_ADD_REPORT = environment.apiUrl + 'v1/digests';
    public static DIGEST_UPDATE_REPORT = environment.apiUrl + 'v1/digests/';
    public static DIGEST_DELETE_REPORT = environment.apiUrl + 'v1/digests/';
    public static DIGEST_GET_REPORT_BY_ID = environment.apiUrl + 'v1/digests/byId/'
    public static DIGEST_GET_ALL_REPORTS = environment.apiUrl + 'v1/digests/all';
    public static DIGEST_GENERATE_HASH = environment.apiUrl + 'v1/digests/hash'
    public static BUSCAR_ACESSO = environment.apiUrl + 'v1/digests/buscarAcessos';

    // Grupo A
    public static GET_DATA_FECHAMENTO = environment.apiUrl + 'v1/grupoa/getDataFechamentoFatura'
    public static SET_DATA_FECHAMENTO = environment.apiUrl + 'v1/grupoa/setDataFechamentoFatura'
    public static PERMISSÃO_GRUPOA = environment.apiUrl + 'v1/usuario/checkPermissaoGrupoA'

    // Favoritos
    public static GET_ALL_FAVORITES = environment.apiUrl + 'v1/usuario/favoritos/listAll'
    public static INSERT_FAVORITE = environment.apiUrl + 'v1/usuario/favoritos/insert'
    public static UPDATE_FAVORITE = environment.apiUrl + 'v1/usuario/favoritos/update'
    public static DELETE_FAVORITE = environment.apiUrl + 'v1/usuario/favoritos/delete'

    // Expurgo
    public static EXPURGO_PUT = environment.apiUrl + 'v1/expurgos/'
    public static EXPURGO_DELETE = environment.apiUrl + 'v1/expurgos/'
    public static EXPURGO_POST = environment.apiUrl + 'v1/expurgos'
    public static EXPURGO_BY_ID = environment.apiUrl + 'v1/expurgos/byId/'
    public static EXPURGO_GET_ALL = environment.apiUrl + 'v1/expurgos/all'

    // Busca por pontos
    public static PONTOS_FILTRO = environment.apiUrl + 'v1/relatorios/relatorioPontosFiltros'
    public static DETALHES_PONTO = environment.apiUrl + 'v1/relatorios/relatorioDetalhesPonto'
    public static TODOS_PRODUTOS = environment.apiUrl + 'v1/ponto/buscarTodosProdutos'
    public static TODOS_CONTRATOS = environment.apiUrl + 'v1/ponto/buscarTodosContratos'

    // Relatórios
    public static RESERVA_CAPACIDADE = environment.apiUrl + 'v1/relatorios/reservaCapacidade';

    // Permissão de Ponto
    public static VERIFICAR_PERMISSAO_PONTO = environment.apiUrl + 'v1/permissoes/permissaoProduto';
}
