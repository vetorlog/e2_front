import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'emeter2-card-resume-info',
  templateUrl: './card-resume-info.component.html',
  styleUrls: ['./card-resume-info.component.scss']
})
export class CardResumeInfoComponent implements OnInit {

  @Input() title: string;
  @Input() logoType: string;
  @Input() numberText: string;
  @Input() subText: string;

  constructor() { }

  ngOnInit(): void {
  }

}
