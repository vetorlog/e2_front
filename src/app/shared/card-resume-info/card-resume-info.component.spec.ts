import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardResumeInfoComponent } from './card-resume-info.component';

describe('CardResumeInfoComponent', () => {
  let component: CardResumeInfoComponent;
  let fixture: ComponentFixture<CardResumeInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardResumeInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardResumeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
