import { Component } from '@angular/core';
import { LoginService } from './security/login/login.service';
import { User } from './security/login/user';

@Component({
  selector: 'emeter2-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'emeter2';
  currentUser: User;

  constructor(private loginService: LoginService) {
    this.loginService.currentUser.subscribe(userReturn => this.currentUser = userReturn);
  }

  ngOnInit() {
  }
}
