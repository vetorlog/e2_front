import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './security/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { MapsComponent } from './shared/maps/maps.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard], canLoad: [AuthGuard] },
  { path: 'data', loadChildren: () => import('./data-center/data-center.module').then(m => m.DataCenterModule), canActivate: [AuthGuard], canLoad: [AuthGuard] },
  { path: 'report', loadChildren: () => import('./reports/report/report.module').then(m => m.ReportModule), canActivate: [AuthGuard], canLoad: [AuthGuard] },
  { path: 'map', component: MapsComponent, canActivate: [AuthGuard], canLoad: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
