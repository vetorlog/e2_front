import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient, private toastr: ToastrService) { }

  private setHeader() {
    const headers = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Accept-Language': 'pt_BR',
      // 'Authorization': (localStorage.getItem('auth') != null
      //   ? JSON.parse(localStorage.getItem('auth')).token : '')
      'Authorization': 'Qjg5MDM4M0ZDRkQwRUU5MDIzQ0RGMzAzNEM='
    });

    return headers;
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage;
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    this.toastr.error(errorMessage, "Erro ao chamar a API");
    return throwError(errorMessage);
  }

  public get(url?: string, params?: HttpParams) {
    let options = {
      headers: this.setHeader(),
      params: params
    };

    return this.httpClient.get(url, options).pipe(
      retry(3),
      catchError(this.handleError));
  }
}
