import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';

@Injectable({
  providedIn: 'root'
})
export class ExcelExportService {

  constructor() { }

  public exportTableDataCollect(dataMainToExport: any, dataResumToExport: any, namefile: string) {
    let keysData = Object.keys(dataMainToExport[0])
    let keysDataResume = Object.keys(dataResumToExport)
    let resumArrayToExport = [];
    resumArrayToExport.push(dataResumToExport);
    keysData = this.removeKeysMainDataCollect(keysData);
    keysDataResume = this.removeKeysResumeDataCollect(keysDataResume);

    //Transformando os valores string em float para trabalhar com número no excel
    let fileMainToExport = this.parseValueTableDataCollect(dataMainToExport, keysData);
    let fileResumeToExport = this.parseValueTableDataCollect(resumArrayToExport, keysDataResume);

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(fileMainToExport);
    const ws2: XLSX.WorkSheet = XLSX.utils.json_to_sheet(fileResumeToExport);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Tabela_Principal');
    XLSX.utils.book_append_sheet(wb, ws2, 'Resumo');

    XLSX.writeFile(wb, namefile);
  }

  parseValueTableDataCollect(dataToExport, keysData) {
    for (var i = 0; i < dataToExport.length; i++) {
      for (var k = 0; k < keysData.length; k++) {
        if (dataToExport[i][keysData[k]] == "null" || dataToExport[i][keysData[k]] == null) {
          dataToExport[i][keysData[k]] = "-"
        } else {
          dataToExport[i][keysData[k]] = parseFloat(dataToExport[i][keysData[k]]);
        }
      }
    }
    return dataToExport;
  }

  removeKeysMainDataCollect(keysData) {
    keysData.splice(keysData.indexOf('created'), 1)
    keysData.splice(keysData.indexOf('dataColeta'), 1)
    keysData.splice(keysData.indexOf('sh'), 1)
    keysData.splice(keysData.indexOf('mail'), 1)
    keysData.splice(keysData.indexOf('id'), 1)
    keysData.splice(keysData.indexOf('time'), 1)
    keysData.splice(keysData.indexOf('flowConsistency'), 1)
    keysData.splice(keysData.indexOf('vertimento'), 1)
    keysData.splice(keysData.indexOf('estimativaContabilizacao'), 1)

    return keysData;
  }

  removeKeysResumeDataCollect(keysData) {
    keysData.splice(keysData.indexOf('created'), 1)
    keysData.splice(keysData.indexOf('dataColeta'), 1)
    keysData.splice(keysData.indexOf('sh'), 1)
    keysData.splice(keysData.indexOf('mail'), 1)
    keysData.splice(keysData.indexOf('id'), 1)
    keysData.splice(keysData.indexOf('time'), 1)
    keysData.splice(keysData.indexOf('flowConsistency'), 1)
    keysData.splice(keysData.indexOf('vertimento'), 1)
    keysData.splice(keysData.indexOf('estimativaContabilizacao'), 1)

    return keysData;
  }
}
