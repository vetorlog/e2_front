import { Injectable, Input } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { ApiService } from './api.service';
import { EndPoint } from '../shared/constants/end-point';
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class FormDatesService {
    constructor(private api: ApiService, private toastr: ToastrService) { }

    async callMeters(siteId) {
        let params = new HttpParams()
            .set('site_id', siteId);

        let dataToReturn = await this.api.get(EndPoint.MEDIDORES, params).toPromise().then(
            data => {
                if (data['sitesMeters'] == null || data['sitesMeters'] == undefined || data['sitesMeters'] == {}) {
                    this.toastr.warning("Não foram encontrados os medidores", "Medidor");
                } else {
                    return data['sitesMeters'];
                }
            },
            err => {
                this.toastr.error("Erro ao buscar os medidores", "Medidor");
                return [];
            });

        return dataToReturn;
    }
}