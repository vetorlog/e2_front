import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { LoginService } from 'src/app/security/login/login.service';
import { HttpParams } from '@angular/common/http';
import { EndPoint } from 'src/app/shared/constants/end-point';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'emeter2-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss']
})
export class HeaderMenuComponent implements OnInit {

  @ViewChild("insideElement") insideElement;
  @HostListener('document:click', ['$event.target'])

  public onClick(targetElement) {
    const clickedInside = this.insideElement.nativeElement.contains(targetElement);
    if (!clickedInside && document.getElementById("dropdownGroupAndPoints").classList.contains("height-dropdown")) {
      this.activeDropdown()
    }
  }

  constructor(private loginService: LoginService, private api: ApiService) { }

  gruposPontos: any;
  search: any;

  ngOnInit(): void {
    this.getGroupsAndPoints();
  }

  logout() {
    this.loginService.logout();
  }

  getGroupsAndPoints() {
    const getItemGruposPontos = sessionStorage.getItem("gruposPontos");
    if (getItemGruposPontos == null) {
      let param = new HttpParams()
        .set("recuperarPontosSemGrupo", "1");

      this.api.get(EndPoint.GRUPOS_PONTOS, param)
        .subscribe(
          data => {
            sessionStorage.setItem("gruposPontos", JSON.stringify(data));
            this.gruposPontos = data;
          },
          error => {
            console.log(error);
          });
    } else {
      this.gruposPontos = JSON.parse(sessionStorage.getItem("gruposPontos"));
    }
  }

  hiddenDropdown() {
    document.getElementById("dropdownGroupAndPoints").classList.remove("height-dropdown");
    document.getElementById("inputDiv").classList.remove("display-block");
  }

  activeDropdown() {
    if (!document.getElementById("dropdownGroupAndPoints").classList.contains("height-dropdown")) {
      document.getElementById("dropdownGroupAndPoints").classList.add("height-dropdown");
      document.getElementById("inputDiv").classList.add("display-block");
    } else {
      document.getElementById("dropdownGroupAndPoints").classList.remove("height-dropdown");
      document.getElementById("inputDiv").classList.remove("display-block");
    }
  }

  showPoints(id) {
    if (document.getElementById("hiddenPoint" + id).classList.contains("show-points")) {
      document.getElementById("hiddenPoint" + id).classList.remove("show-points");
      document.getElementById("groupLi" + id).classList.remove("li-active");
      document.getElementById("nameGroup" + id).classList.remove("active-group");
      document.getElementById("hiddenPoint" + id).classList.remove("bd-top-active");
    } else {
      document.getElementById("hiddenPoint" + id).classList.add("show-points");
      document.getElementById("groupLi" + id).classList.add("li-active");
      document.getElementById("nameGroup" + id).classList.add("active-group");
      document.getElementById("hiddenPoint" + id).classList.add("bd-top-active");
    }
  }

}
