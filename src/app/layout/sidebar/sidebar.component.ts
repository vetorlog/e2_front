import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { EndPoint } from 'src/app/shared/constants/end-point';
import { HttpParams } from '@angular/common/http';
import { LoginService } from 'src/app/security/login/login.service';

@Component({
  selector: 'emeter2-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(private api: ApiService, private login: LoginService) { }

  ngOnInit(): void {
  }

  callLogout() {
    this.login.logout();
  }

}
