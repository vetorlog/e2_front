import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu/menu.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderMenuComponent } from './header-menu/header-menu.component';
import { AngularMaterialModule } from 'src/assets/angular-material-module/angular-material.module';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    MenuComponent,
    SidebarComponent,
    HeaderMenuComponent,
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    MenuComponent,
    SidebarComponent,
    HeaderMenuComponent
  ]
})
export class LayoutModule { }
