import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from './user';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { EndPoint } from 'src/app/shared/constants/end-point';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class LoginService {

    public currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    private authenticatedUser: boolean = false;
    private headers = new HttpHeaders();

    constructor(private router: Router, private http: HttpClient, private toastr: ToastrService) {
        this.headers.append('Accept', 'application/json');
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept-Language', 'pt_BR');
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    userIsAuthenticated() {
        return this.authenticatedUser;
    }

    async login(user: User) {
        return await this.authenticateCredentials(user);
    }

    logout() {
        this.currentUserSubject.next(null);
        sessionStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
    }

    authenticateCredentials(user: User) {
        const credentials = {
            credenciais: btoa(user.name + ':' + user.password),
            aplicacao: '5'
        };
        return this.http.post(EndPoint.API_AUTHENTICATION, credentials).toPromise().then(
            (authenticateReturn: AuthenticateModel) => {
                sessionStorage.setItem('currentUser', JSON.stringify(authenticateReturn.token));
                this.currentUserSubject.next(user);
                this.router.navigate(['/']);
                return "Success Login";
            },
            err => {
                this.toastr.error(err.error, "Erro ao realizar o Login");
                return "Err Login";
            }
        )
    }
}

class AuthenticateModel {
    token: string;
}