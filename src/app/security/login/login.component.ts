import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { User } from './user';

@Component({
  selector: 'emeter2-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: User = new User();

  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
  }

  async login() {
    document.getElementById("loginBtn").classList.add("active");
    const returnLoginMessage = await this.loginService.login(this.user);

    // o IF está redundante pois faz a mesma coisa, porém, quis deixar para remover
    // em ambos os casos e tratei o retorno para caso precisar realizar melhorias futuras na aplicação
    if (returnLoginMessage.toLowerCase() == "success login") {
      document.getElementById("loginBtn").classList.remove("active");
    } else {
      document.getElementById("loginBtn").classList.remove("active");
    }
  }

}
